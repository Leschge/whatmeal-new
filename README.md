# HOW TO START


1. Go into 'dev-backend' and run via 'npm start'
2. Open browser with url 'http://localhost:7000/'

There exists an account to test:
username: testuser
password: 123

All features that are visible on the website are working, such as register a new user, login, add recipe / rating, ...

IDEA BEHIND THIS PROJECT

the website is meant to publish and rate recipes. Registered people can filter this recipes based on their current fridge. So that they know what meal can be prepared with the ingredients that are at home.



# WHATMEAL

Deploy Angular with Express Tut:
https://www.youtube.com/watch?v=sTbQphoYbK0

Json Validator:
https://jsonformatter.curiousconcept.com/

# GIT COMMANDS

`git pull origin master` downloads latest version from master

`git add <filename>` | `git add *` adds file to staging status

`git commit -m "Commit message"` | `git commit -a` Commit changes to head | Commit any files you've added with git add, and also commit any files you've changed since then:

`git push origin master` Send changes to the master branch

`git fetch origin` Instead, to drop all your local changes and commits, fetch the latest history from the server and point your local master branch at it

# FIRST STEPS


**IT SHOULD BE ENOUGH TO RUN**

`git clone https://gitlab.com/Leschge/whatmeal-new.git` clone project to current folder

`cd dev-backend` | `cd dev-frontend` go to development folder

`npm install`   installs all modules/packages that are defined in the *package.json*




**OTHERWISE THIS MAY HELPS:**

1. Set up proxy in npm 

    `npm config set proxy http://web-proxy.bbn.hpecorp.net:8080`

    `npm condif set https-proxy http://web-proxy.bbn.hpecorp.net:8080`

2. Install express

    `npm install express --save`
    
3. Install Gulp Stuff

    `npm install --global gulp-cli`

    `npm install node-sass gulp-sass --save-dev`
    
    `npm install gulp-cssnano --save-dev`
    
    `npm install gulp-concat --save-dev`
    
    `npm install gulp-terser --save-dev`
    
    `npm install gulp-typescript typescript --save-dev`
    
    
4. Install typescript

    `npm install -g typescript`
    

    
    
    
# KNOWN PROBLEMS & SOLUTIONS

Problem:

> ERROR in The Angular Compiler requires TypeScript >=3.4.0 and <3.6.0 but 3.7.2 was found instead. 

Solution:

`npm install typescript@">=3.4.0 <3.6.0"`