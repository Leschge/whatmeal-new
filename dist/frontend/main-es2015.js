(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n  \n  <nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\n    <a class=\"navbar-brand\" routerLink=\"\" >WhatMeal.com</a>\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n      <span class=\"navbar-toggler-icon\"></span>\n    </button>\n    \n    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\n\n      <ul class=\"navbar-nav mr-auto\">\n        <li class=\"nav-item active\">\n          <a class=\"nav-link\" routerLink=\"/\" >Home<span class=\"sr-only\">(current)</span></a>\n        </li>\n        <li class=\"nav-item\">\n          <a class=\"nav-link\" routerLink=\"/account\" >Account</a>\n        </li>\n        <li class=\"nav-item\">\n            <a class=\"nav-link\" routerLink=\"/register\" >Register</a>\n          </li>\n\n        <li class=\"nav-item dropdown\">\n          <a class=\"nav-link dropdown-toggle\" routerLink=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n            About\n          </a>\n          <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\n            <a class=\"dropdown-item\" routerLink=\"#\" >Imprint</a>\n            <a class=\"dropdown-item\" routerLink=\"#\" >Report a bug</a>\n          </div>\n        </li>\n      </ul>\n\n    </div>\n  </nav> \n  \n  <router-outlet></router-outlet>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/fridge/fridge.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/fridge/fridge.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p style=\"text-align: center;\">FRIDGE COMPONENT</p>\n\n\n<p class=\"headline\">Your Fridge</p>\n<input [(ngModel)]=\"searchFridgeTerm\" (ngModelChange)=\"searchFridge()\" class=\"form-control form-control-sm\" id=\"formSearchIng\" type=\"text\" placeholder=\"Search your Fridge\">\n    <div class=\"list-container\">\n        <ul class=\"list-group\">\n            <app-ingredient *ngFor=\"let item of allItems\" [item]=\"item\"></app-ingredient>\n                <div *ngIf=\"loadingFridge\" class=\"text-center\">\n                    <div class=\"spinner-grow text-success\" role=\"status\">\n                        <span class=\"sr-only\">Loading...</span>\n                    </div>\n                </div>\n        </ul>\n    </div>\n\n<p class=\"headline\">Ingredients Database</p>\n<input [(ngModel)]=\"searchIngredientTerm\" (ngModelChange)=\"searchIngredient()\" class=\"form-control form-control-sm\" id=\"formSearchIng\" type=\"text\" placeholder=\"Search Ingredient to add to Fridge\">\n    <div class=\"list-container\">\n        <ul class=\"list-group\">\n            <app-ingredient-add *ngFor=\"let ingredient of allIngredients\" [ingredient]=\"ingredient\"></app-ingredient-add>\n            <div *ngIf=\"loadingIngredients\" class=\"text-center\">\n                <div class=\"spinner-grow text-success\" role=\"status\">\n                    <span class=\"sr-only\">Loading...</span>\n                </div>\n            </div>\n        </ul>\n    </div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/ingredient-add/ingredient-add.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/ingredient-add/ingredient-add.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"ingred-comp\">\n    <label class=\"ingredient-grid\">{{ingredient.name}}</label>\n    <label class=\"ingredient-grid\">\n        <input [(ngModel)]=\"amount\" placeholder=\"amount\" class=\"ingredient-amount\"/>\n        {{ingredient.unit}}\n    </label>\n    <label (click)=\"addItemToFridge()\" class=\"ingredient-grid button\"><button type=\"submit\" class=\"btn btn-success btn-sm\">➕</button></label>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/ingredient-recipe/ingredient-recipe.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/ingredient-recipe/ingredient-recipe.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<div class=\"ingred-container\">\n    <label class=\"ingredient-name\">{{ingredient.name}}</label>\n    <label class=\"ingredient-amount\">{{ingredient.amount}} {{ingredient.unit}}</label>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/ingredient/ingredient.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/ingredient/ingredient.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"ingred-comp\">\n    <label class=\"ingredient-grid name\">{{item.name}}</label>\n    <label class=\"ingredient-grid amount\">\n        <input [(ngModel)]=\"item.amount\" (ngModelChange)=\"fridgeChangeAmount()\" placeholder=\"amount\" class=\"ingredient-amount\"/>\n        {{item.unit}}\n    </label>\n    <label class=\"ingredient-grid date\">🤢⌛</label>\n    <label (click)=\"delItemFromFridge()\" class=\"ingredient-grid button\"><button type=\"submit\" class=\"btn btn-danger btn-sm\">🗑️</button></label>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login-form/login-form.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login-form/login-form.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-sm\"></div>\n            <div class=\"col-sm\">\n                <form class=\"mx-auto text-center\">\n                    <div class=\"form-group\">\n                        <label for=\"formLoginUser\">Username</label>\n                        <input [(ngModel)]=\"username\" name=\"username\" type=\"Username\" class=\"form-control\" id=\"formLogUser\" placeholder=\"Username\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"formRegUser\">Password</label>\n                        <input [(ngModel)]=\"password\" name=\"password\" type=\"password\" class=\"form-control\" id=\"formLogPw\" placeholder=\"Password\">\n                    </div>\n                    <button (click)=\"login()\" type=\"submit\" class=\"btn btn-success\">LOGIN</button>\n                </form>\n            </div>\n            <div class=\"col-sm\"></div>\n        </div>\n    </div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/mainpage/mainpage.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/mainpage/mainpage.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p style=\"text-align: center;\">MAINAPP COMPONENT</p>\n\n\n<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md affix\">\n            <app-fridge></app-fridge>\n        </div>\n        <div class=\"col-md\">\n            <app-recipe-container></app-recipe-container>\n        </div>\n    </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/rating/rating.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rating/rating.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container\">\n    <h5><app-stars [stars]=\"rating.stars\"></app-stars>  {{rating.title}}</h5>\n    <label class=\"gray\">{{rating.date}}&nbsp;&nbsp;·&nbsp;&nbsp;{{rating.user}}</label> \n    <div>{{rating.description}}</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/recipe-container/recipe-container.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/recipe-container/recipe-container.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p style=\"text-align: center;\">RECIPE CONTAINER COMPONENT</p>\n<p class=\"headline\">Recipies</p>\n<input [(ngModel)]=\"searchRecipeTerm\" (ngModelChange)=\"searchRecipe()\" class=\"form-control form-control-sm\" id=\"formSearchIng\" type=\"text\" placeholder=\"Search for a Recipe\">\n\n<div>\n    <label class=\"dur-label\" for=\"dur-selector\">max. 🕑 {{selDur}} min</label>\n    <input id=\"dur-selector\" type=\"range\" class=\"custom-range\" min=\"5\" value={{selDur}} max={{maxDur}} #dur (input)=\"filterDuration(dur.value)\">\n</div>\n\n<div>\n    <label class=\"diff-label\" for=\"dur-selector\">max. 👨‍🍳 {{symDiff[selDiff]}} </label>\n    <input id=\"diff-selector\" type=\"range\" class=\"custom-range\" min=\"1\" value={{selDiff}} max={{maxDiff}} #diff (input)=\"filterDifficulty(diff.value)\">\n</div>\n\n<div class=\"list-container\">\n    <div class=\"list-group\">\n        <app-recipe *ngFor=\"let recipe of allRecipes\" [recipe]=\"recipe\" routerLink=\"/recipe/{{recipe.ID}}\"></app-recipe>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/recipe-detail/recipe-detail.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/recipe-detail/recipe-detail.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"center\">\n\n    <app-recipe [recipe]=\"recipe\"></app-recipe>\n\n    <div class=\"row\">\n        <div class=\"col-md tablestyle\">\n            <app-ingredient-recipe *ngFor=\"let item of allItems\" [ingredient]=\"item\"></app-ingredient-recipe>\n        </div>\n\n        <div class=\"col-md-7\">\n            <h4>Preparation</h4>\n            {{recipe.preparation}}\n        </div>\n    </div>\n\n    <div class=\"rating-container\">\n        <app-rating></app-rating>\n        <app-rating></app-rating>\n        <app-rating></app-rating>\n    </div>\n    \n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/recipe/recipe.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/recipe/recipe.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"recipe-comp\">\n    <img class=\"mx-auto d-block\" src={{image}} alt=\"dish picture\">\n    <h4 class=\"pl-3\">{{recipe.title}}</h4>\n    <label title=\"rating\" class=\"recipe-label\"><app-stars></app-stars></label>\n    <label title=\"preparation time\" class=\"recipe-label\" >🕑 {{recipe.preparationtime}}min</label>\n    <label title=\"difficulty\" class=\"recipe-label\" >👨‍🍳 {{symDiff[recipe.difficulty]}}</label>\n    <label title=\"origin\" class=\"recipe-label\">{{recipe.origin}}</label>\n    <br>\n    <label class=\"mx-auto d-block text-center\" >{{recipe.description}}</label>\n    <label class=\"mx-auto d-block text-center font-italic font-weight-light\"><span  *ngFor=\"let tag of recipe.tags\"> {{tag}} </span></label>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/register-form/register-form.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/register-form/register-form.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-sm\"></div>\n        <div class=\"col-sm\">\n            <form ngNativeValidate class=\"mx-auto text-center\">\n                <div class=\"form-group\">\n                    <label for=\"formRegMail\">Email address</label>\n                    <input [(ngModel)]=\"email\" name=\"email\" required type=\"email\" class=\"form-control\" id=\"formRegMail\" aria-describedby=\"emailHelp\" placeholder=\"EMail\">\n                    <small id=\"emailHelp\" class=\"form-text text-muted\">We'll never share your email with anyone else.</small>\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"formRegUser\">Username</label>\n                    <input [(ngModel)]=\"username\" name=\"username\" required type=\"name\" class=\"form-control\" id=\"formRegUser\" aria-describedby=\"emailHelp\" placeholder=\"Username\">\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"formRegPw\">Password</label>\n                    <input [(ngModel)]=\"password\" name=\"password\" required type=\"password\" class=\"form-control\" id=\"formRegPw\" placeholder=\"Password\">\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"formRegPwCon\">Confirm Password</label>\n                    <input [(ngModel)]=\"passwordrep\" name=\"passwordrep\" required type=\"password\" class=\"form-control\" id=\"formRegPwCon\" placeholder=\"Password\">\n                </div>\n\n                <button (click)=\"register()\" type=\"submit\" class=\"btn btn-success\">REGISTER</button>\n            </form>\n        </div>\n        <div class=\"col-sm\"></div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/stars/stars.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/stars/stars.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<span class=\"star\" [ngStyle]=\"{'color': col[0]}\">★</span>\n<span class=\"star\" [ngStyle]=\"{'color': col[1]}\">★</span>\n<span class=\"star\" [ngStyle]=\"{'color': col[2]}\">★</span>\n<span class=\"star\" [ngStyle]=\"{'color': col[3]}\">★</span>\n<span class=\"star\" [ngStyle]=\"{'color': col[4]}\">★</span>");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./register-form/register-form.component */ "./src/app/register-form/register-form.component.ts");
/* harmony import */ var _login_form_login_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login-form/login-form.component */ "./src/app/login-form/login-form.component.ts");
/* harmony import */ var _mainpage_mainpage_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./mainpage/mainpage.component */ "./src/app/mainpage/mainpage.component.ts");
/* harmony import */ var _recipe_detail_recipe_detail_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./recipe-detail/recipe-detail.component */ "./src/app/recipe-detail/recipe-detail.component.ts");







const routes = [
    { path: '', component: _mainpage_mainpage_component__WEBPACK_IMPORTED_MODULE_5__["MainpageComponent"] },
    { path: 'register', component: _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_3__["RegisterFormComponent"] },
    { path: 'account', component: _login_form_login_form_component__WEBPACK_IMPORTED_MODULE_4__["LoginFormComponent"] },
    { path: 'login', component: _login_form_login_form_component__WEBPACK_IMPORTED_MODULE_4__["LoginFormComponent"] },
    { path: 'recipe/:ID', component: _recipe_detail_recipe_detail_component__WEBPACK_IMPORTED_MODULE_6__["RecipeDetailComponent"] },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'dev-frontend';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./register-form/register-form.component */ "./src/app/register-form/register-form.component.ts");
/* harmony import */ var _login_form_login_form_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./login-form/login-form.component */ "./src/app/login-form/login-form.component.ts");
/* harmony import */ var _fridge_fridge_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./fridge/fridge.component */ "./src/app/fridge/fridge.component.ts");
/* harmony import */ var _ingredient_ingredient_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./ingredient/ingredient.component */ "./src/app/ingredient/ingredient.component.ts");
/* harmony import */ var _mainpage_mainpage_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./mainpage/mainpage.component */ "./src/app/mainpage/mainpage.component.ts");
/* harmony import */ var _ingredient_add_ingredient_add_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./ingredient-add/ingredient-add.component */ "./src/app/ingredient-add/ingredient-add.component.ts");
/* harmony import */ var _recipe_recipe_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./recipe/recipe.component */ "./src/app/recipe/recipe.component.ts");
/* harmony import */ var _recipe_container_recipe_container_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./recipe-container/recipe-container.component */ "./src/app/recipe-container/recipe-container.component.ts");
/* harmony import */ var _recipe_detail_recipe_detail_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./recipe-detail/recipe-detail.component */ "./src/app/recipe-detail/recipe-detail.component.ts");
/* harmony import */ var _ingredient_recipe_ingredient_recipe_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./ingredient-recipe/ingredient-recipe.component */ "./src/app/ingredient-recipe/ingredient-recipe.component.ts");
/* harmony import */ var _rating_rating_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./rating/rating.component */ "./src/app/rating/rating.component.ts");
/* harmony import */ var _stars_stars_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./stars/stars.component */ "./src/app/stars/stars.component.ts");



















let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
            _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_7__["RegisterFormComponent"],
            _login_form_login_form_component__WEBPACK_IMPORTED_MODULE_8__["LoginFormComponent"],
            _fridge_fridge_component__WEBPACK_IMPORTED_MODULE_9__["FridgeComponent"],
            _ingredient_ingredient_component__WEBPACK_IMPORTED_MODULE_10__["IngredientComponent"],
            _mainpage_mainpage_component__WEBPACK_IMPORTED_MODULE_11__["MainpageComponent"],
            _ingredient_add_ingredient_add_component__WEBPACK_IMPORTED_MODULE_12__["IngredientAddComponent"],
            _recipe_recipe_component__WEBPACK_IMPORTED_MODULE_13__["RecipeComponent"],
            _recipe_container_recipe_container_component__WEBPACK_IMPORTED_MODULE_14__["RecipeContainerComponent"],
            _recipe_detail_recipe_detail_component__WEBPACK_IMPORTED_MODULE_15__["RecipeDetailComponent"],
            _ingredient_recipe_ingredient_recipe_component__WEBPACK_IMPORTED_MODULE_16__["IngredientRecipeComponent"],
            _rating_rating_component__WEBPACK_IMPORTED_MODULE_17__["RatingComponent"],
            _stars_stars_component__WEBPACK_IMPORTED_MODULE_18__["StarsComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"]
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/fridge/fridge.component.css":
/*!*********************************************!*\
  !*** ./src/app/fridge/fridge.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".headline{\r\n    background-color: lightgray;\r\n    padding: 0.2em;\r\n    border-radius: 5px;\r\n    text-align: center;\r\n    margin-bottom: 0.2em;\r\n}\r\n\r\n.list-container{\r\n    border: 1px solid lightgray;\r\n    border-radius: 5px;\r\n    margin-bottom: 0.2em;\r\n    margin-top: 0.2em;\r\n    max-height: 12em;\r\n    overflow-y: scroll;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZnJpZGdlL2ZyaWRnZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksMkJBQTJCO0lBQzNCLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLG9CQUFvQjtBQUN4Qjs7QUFFQTtJQUNJLDJCQUEyQjtJQUMzQixrQkFBa0I7SUFDbEIsb0JBQW9CO0lBQ3BCLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0FBQ3RCIiwiZmlsZSI6InNyYy9hcHAvZnJpZGdlL2ZyaWRnZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRsaW5le1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmF5O1xyXG4gICAgcGFkZGluZzogMC4yZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwLjJlbTtcclxufVxyXG5cclxuLmxpc3QtY29udGFpbmVye1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMC4yZW07XHJcbiAgICBtYXJnaW4tdG9wOiAwLjJlbTtcclxuICAgIG1heC1oZWlnaHQ6IDEyZW07XHJcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/fridge/fridge.component.ts":
/*!********************************************!*\
  !*** ./src/app/fridge/fridge.component.ts ***!
  \********************************************/
/*! exports provided: FridgeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FridgeComponent", function() { return FridgeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _mock_fridge_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../mock-fridge.json */ "./src/app/mock-fridge.json");
var _mock_fridge_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../mock-fridge.json */ "./src/app/mock-fridge.json", 1);
/* harmony import */ var _http_requests_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../http-requests.service */ "./src/app/http-requests.service.ts");




let FridgeComponent = class FridgeComponent {
    constructor(httpService) {
        this.httpService = httpService;
        this.allProducts = _mock_fridge_json__WEBPACK_IMPORTED_MODULE_2__.products; // All products (ingredients) from users fridge
        this.searchFridgeTerm = "";
        this.searchIngredientTerm = "";
        this.loadingFridge = true;
        this.loadingIngredients = true;
    }
    ngOnInit() {
        this.getIngredients();
        /*
        console.log(this.allProducts)
        console.log(this.allIngredients);
        console.log(this.allItems);*/
    }
    getIngredients() {
        this.httpService.getIngredients().subscribe(ings => {
            this.allIngredients = ings;
            this.allIngredientsCopy = this.allIngredients;
            this.allItems = this.allProducts.map((item, i) => Object.assign({}, item, this.allIngredients[i]));
            this.loadingFridge = false;
            this.loadingIngredients = false;
        });
    }
    searchFridge() {
        let term = this.searchFridgeTerm;
        this.allItems = this.allItemsCopy.filter(function (obj) {
            return obj.name.indexOf(term) >= 0;
        });
    }
    searchIngredient() {
        let term = this.searchIngredientTerm;
        this.allIngredients = this.allIngredientsCopy.filter(function (obj) {
            return obj.name.indexOf(term) >= 0;
        });
    }
};
FridgeComponent.ctorParameters = () => [
    { type: _http_requests_service__WEBPACK_IMPORTED_MODULE_3__["HttpRequestsService"] }
];
FridgeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fridge',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./fridge.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/fridge/fridge.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./fridge.component.css */ "./src/app/fridge/fridge.component.css")).default]
    })
], FridgeComponent);



/***/ }),

/***/ "./src/app/http-requests.service.ts":
/*!******************************************!*\
  !*** ./src/app/http-requests.service.ts ***!
  \******************************************/
/*! exports provided: HttpRequestsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpRequestsService", function() { return HttpRequestsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_Http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/Http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let HttpRequestsService = class HttpRequestsService {
    constructor(http) {
        this.http = http;
        this.ingredientsUrl = "api/ingredient/getAll";
        this.loginUrl = "api/user/login";
        this.registerUrl = "api/user/register";
        this.getUserIdUrl = "api/user/getMyUserID";
    }
    getIngredients() {
        console.log("SERVICE: Ingredient: fetched ingredients from database...");
        return this.http.get(this.ingredientsUrl)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError("getIngredients", [])));
    }
    // LOGIN FUNCTIONS //
    login(username, password) {
        console.log("SERVICE: Login: loggin in ...");
        return this.http.post(this.loginUrl, { username, password })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError("login")));
    }
    getUserId() {
        console.log("SERVICE: getUserId: ..");
        return this.http.get(this.getUserIdUrl)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError("getUserId")));
    }
    // REGISTER FUNCTIONS //
    register(username, email, password) {
        console.log("SERVICE: Register: ...");
        return this.http.post(this.registerUrl, { username, email, password })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError("register")));
    }
    // ERROR //
    handleError(operation = "operation", result) {
        return (error) => {
            console.error(error);
            console.log(`${operation} failed: ${error.message}`);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(result);
        };
    }
};
HttpRequestsService.ctorParameters = () => [
    { type: _angular_common_Http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
HttpRequestsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], HttpRequestsService);



/***/ }),

/***/ "./src/app/ingredient-add/ingredient-add.component.css":
/*!*************************************************************!*\
  !*** ./src/app/ingredient-add/ingredient-add.component.css ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".ingred-comp{\r\n    padding: 0.2em;\r\n    padding-left: 0.5em;\r\n}\r\n\r\n.ingredient-grid{\r\n    width: 33%;\r\n    margin:0px;\r\n}\r\n\r\n.ingredient-amount{\r\n    padding-right: 0.2em;\r\n    text-align: right;\r\n    border: 1px solid lightgray;\r\n    width:70%;\r\n}\r\n\r\n.ingredient-amount:hover{\r\n    border: 1px solid palegreen;\r\n}\r\n\r\n.button{\r\n    text-align: right;\r\n    padding: 0px;\r\n    margin: 0px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5ncmVkaWVudC1hZGQvaW5ncmVkaWVudC1hZGQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGNBQWM7SUFDZCxtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxVQUFVO0lBQ1YsVUFBVTtBQUNkOztBQUVBO0lBQ0ksb0JBQW9CO0lBQ3BCLGlCQUFpQjtJQUNqQiwyQkFBMkI7SUFDM0IsU0FBUztBQUNiOztBQUVBO0lBQ0ksMkJBQTJCO0FBQy9COztBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixXQUFXO0FBQ2YiLCJmaWxlIjoic3JjL2FwcC9pbmdyZWRpZW50LWFkZC9pbmdyZWRpZW50LWFkZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmluZ3JlZC1jb21we1xyXG4gICAgcGFkZGluZzogMC4yZW07XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDAuNWVtO1xyXG59XHJcblxyXG4uaW5ncmVkaWVudC1ncmlke1xyXG4gICAgd2lkdGg6IDMzJTtcclxuICAgIG1hcmdpbjowcHg7XHJcbn1cclxuXHJcbi5pbmdyZWRpZW50LWFtb3VudHtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDAuMmVtO1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XHJcbiAgICB3aWR0aDo3MCU7XHJcbn1cclxuXHJcbi5pbmdyZWRpZW50LWFtb3VudDpob3ZlcntcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHBhbGVncmVlbjtcclxufVxyXG5cclxuLmJ1dHRvbntcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgcGFkZGluZzogMHB4O1xyXG4gICAgbWFyZ2luOiAwcHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/ingredient-add/ingredient-add.component.ts":
/*!************************************************************!*\
  !*** ./src/app/ingredient-add/ingredient-add.component.ts ***!
  \************************************************************/
/*! exports provided: IngredientAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IngredientAddComponent", function() { return IngredientAddComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let IngredientAddComponent = class IngredientAddComponent {
    constructor() {
        this.amount = 10;
    }
    ngOnInit() {
        switch (this.ingredient.unit) {
            case "g":
                this.amount = 100;
                break;
            case "ml":
                this.amount = 500;
                break;
            case "pcs":
                this.amount = 3;
                break;
            case "g":
                this.amount = 100;
                break;
        }
    }
    addItemToFridge() {
        console.log("Adding " + this.amount + " " + this.ingredient.unit + " " + this.ingredient.name + " to fridge...");
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], IngredientAddComponent.prototype, "ingredient", void 0);
IngredientAddComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ingredient-add',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./ingredient-add.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/ingredient-add/ingredient-add.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./ingredient-add.component.css */ "./src/app/ingredient-add/ingredient-add.component.css")).default]
    })
], IngredientAddComponent);



/***/ }),

/***/ "./src/app/ingredient-recipe/ingredient-recipe.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/ingredient-recipe/ingredient-recipe.component.css ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".ingredient-name{\r\n    width:50%;\r\n    text-align: right;\r\n    padding-right: 1em;\r\n}\r\n\r\n.ingredient-amount{\r\n    width:50%;\r\n    padding-left: 0.5em;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5ncmVkaWVudC1yZWNpcGUvaW5ncmVkaWVudC1yZWNpcGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFNBQVM7SUFDVCxpQkFBaUI7SUFDakIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksU0FBUztJQUNULG1CQUFtQjtBQUN2QiIsImZpbGUiOiJzcmMvYXBwL2luZ3JlZGllbnQtcmVjaXBlL2luZ3JlZGllbnQtcmVjaXBlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW5ncmVkaWVudC1uYW1le1xyXG4gICAgd2lkdGg6NTAlO1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxZW07XHJcbn1cclxuXHJcbi5pbmdyZWRpZW50LWFtb3VudHtcclxuICAgIHdpZHRoOjUwJTtcclxuICAgIHBhZGRpbmctbGVmdDogMC41ZW07XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/ingredient-recipe/ingredient-recipe.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/ingredient-recipe/ingredient-recipe.component.ts ***!
  \******************************************************************/
/*! exports provided: IngredientRecipeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IngredientRecipeComponent", function() { return IngredientRecipeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let IngredientRecipeComponent = class IngredientRecipeComponent {
    constructor() { }
    ngOnInit() {
        console.log(this.ingredient);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], IngredientRecipeComponent.prototype, "ingredient", void 0);
IngredientRecipeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ingredient-recipe',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./ingredient-recipe.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/ingredient-recipe/ingredient-recipe.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./ingredient-recipe.component.css */ "./src/app/ingredient-recipe/ingredient-recipe.component.css")).default]
    })
], IngredientRecipeComponent);



/***/ }),

/***/ "./src/app/ingredient/ingredient.component.css":
/*!*****************************************************!*\
  !*** ./src/app/ingredient/ingredient.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".ingred-comp{\r\n    padding: 0.2em;\r\n    padding-left: 0.5em;\r\n}\r\n\r\n.ingredient-grid{\r\n    width: 25%;\r\n    margin:0px;\r\n}\r\n\r\n.ingredient-amount{\r\n    padding-right: 0.2em;\r\n    text-align: right;\r\n    border: 1px solid lightgray;\r\n    width:70%;\r\n}\r\n\r\n.ingredient-amount:hover{\r\n    border: 1px solid palegreen;\r\n}\r\n\r\n.date{\r\n    text-align: right;\r\n}\r\n\r\n.name{\r\n    width: 33%;;\r\n    padding: 0px;\r\n    margin: 0px;\r\n}\r\n\r\n.amount{\r\n    width: 33%;;\r\n    padding: 0px;\r\n    margin: 0px;\r\n}\r\n\r\n.date{\r\n    width: 16%;\r\n    text-align: right;\r\n    padding: 0px;\r\n    margin: 0px;\r\n    color: gray;\r\n}\r\n\r\n.button{\r\n    width: 16%;\r\n    text-align: right;\r\n    padding: 0px;\r\n    margin: 0px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5ncmVkaWVudC9pbmdyZWRpZW50LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxjQUFjO0lBQ2QsbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksVUFBVTtJQUNWLFVBQVU7QUFDZDs7QUFFQTtJQUNJLG9CQUFvQjtJQUNwQixpQkFBaUI7SUFDakIsMkJBQTJCO0lBQzNCLFNBQVM7QUFDYjs7QUFFQTtJQUNJLDJCQUEyQjtBQUMvQjs7QUFFQTtJQUNJLGlCQUFpQjtBQUNyQjs7QUFHQTtJQUNJLFVBQVU7SUFDVixZQUFZO0lBQ1osV0FBVztBQUNmOztBQUVBO0lBQ0ksVUFBVTtJQUNWLFlBQVk7SUFDWixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxVQUFVO0lBQ1YsaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixXQUFXO0lBQ1gsV0FBVztBQUNmOztBQUVBO0lBQ0ksVUFBVTtJQUNWLGlCQUFpQjtJQUNqQixZQUFZO0lBQ1osV0FBVztBQUNmIiwiZmlsZSI6InNyYy9hcHAvaW5ncmVkaWVudC9pbmdyZWRpZW50LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW5ncmVkLWNvbXB7XHJcbiAgICBwYWRkaW5nOiAwLjJlbTtcclxuICAgIHBhZGRpbmctbGVmdDogMC41ZW07XHJcbn1cclxuXHJcbi5pbmdyZWRpZW50LWdyaWR7XHJcbiAgICB3aWR0aDogMjUlO1xyXG4gICAgbWFyZ2luOjBweDtcclxufVxyXG5cclxuLmluZ3JlZGllbnQtYW1vdW50e1xyXG4gICAgcGFkZGluZy1yaWdodDogMC4yZW07XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcclxuICAgIHdpZHRoOjcwJTtcclxufVxyXG5cclxuLmluZ3JlZGllbnQtYW1vdW50OmhvdmVye1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcGFsZWdyZWVuO1xyXG59XHJcblxyXG4uZGF0ZXtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcblxyXG5cclxuLm5hbWV7XHJcbiAgICB3aWR0aDogMzMlOztcclxuICAgIHBhZGRpbmc6IDBweDtcclxuICAgIG1hcmdpbjogMHB4O1xyXG59XHJcblxyXG4uYW1vdW50e1xyXG4gICAgd2lkdGg6IDMzJTs7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICBtYXJnaW46IDBweDtcclxufVxyXG5cclxuLmRhdGV7XHJcbiAgICB3aWR0aDogMTYlO1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICBtYXJnaW46IDBweDtcclxuICAgIGNvbG9yOiBncmF5O1xyXG59XHJcblxyXG4uYnV0dG9ue1xyXG4gICAgd2lkdGg6IDE2JTtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgcGFkZGluZzogMHB4O1xyXG4gICAgbWFyZ2luOiAwcHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/ingredient/ingredient.component.ts":
/*!****************************************************!*\
  !*** ./src/app/ingredient/ingredient.component.ts ***!
  \****************************************************/
/*! exports provided: IngredientComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IngredientComponent", function() { return IngredientComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let IngredientComponent = class IngredientComponent {
    constructor() { }
    ngOnInit() {
        //console.log(this.item);
    }
    fridgeChangeAmount() {
        console.log("changed amount of " + this.item.name + " to " + this.item.amount);
    }
    delItemFromFridge() {
        console.log("removing " + this.item.name + " from fridge...");
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], IngredientComponent.prototype, "item", void 0);
IngredientComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ingredient',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./ingredient.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/ingredient/ingredient.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./ingredient.component.css */ "./src/app/ingredient/ingredient.component.css")).default]
    })
], IngredientComponent);



/***/ }),

/***/ "./src/app/login-form/login-form.component.css":
/*!*****************************************************!*\
  !*** ./src/app/login-form/login-form.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luLWZvcm0vbG9naW4tZm9ybS5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/login-form/login-form.component.ts":
/*!****************************************************!*\
  !*** ./src/app/login-form/login-form.component.ts ***!
  \****************************************************/
/*! exports provided: LoginFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginFormComponent", function() { return LoginFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _http_requests_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../http-requests.service */ "./src/app/http-requests.service.ts");



let LoginFormComponent = class LoginFormComponent {
    constructor(httpService) {
        this.httpService = httpService;
        this.username = "";
        this.password = "";
    }
    ngOnInit() {
    }
    login() {
        console.log("PARAMS: " + this.username + " " + this.password);
        this.httpService.login(this.username, this.password).subscribe(res => {
            console.log(res);
            if (res.error) {
                if (res.error.indexOf('already logged in') > -1) {
                    this.getUserId();
                }
            }
        });
    }
    getUserId() {
        this.httpService.getUserId().subscribe(res => {
            console.log(res);
        });
    }
};
LoginFormComponent.ctorParameters = () => [
    { type: _http_requests_service__WEBPACK_IMPORTED_MODULE_2__["HttpRequestsService"] }
];
LoginFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login-form',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login-form.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login-form/login-form.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login-form.component.css */ "./src/app/login-form/login-form.component.css")).default]
    })
], LoginFormComponent);



/***/ }),

/***/ "./src/app/mainpage/mainpage.component.css":
/*!*************************************************!*\
  !*** ./src/app/mainpage/mainpage.component.css ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21haW5wYWdlL21haW5wYWdlLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/mainpage/mainpage.component.ts":
/*!************************************************!*\
  !*** ./src/app/mainpage/mainpage.component.ts ***!
  \************************************************/
/*! exports provided: MainpageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainpageComponent", function() { return MainpageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let MainpageComponent = class MainpageComponent {
    constructor() { }
    ngOnInit() {
    }
};
MainpageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-mainpage',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./mainpage.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/mainpage/mainpage.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./mainpage.component.css */ "./src/app/mainpage/mainpage.component.css")).default]
    })
], MainpageComponent);



/***/ }),

/***/ "./src/app/mock-fridge.json":
/*!**********************************!*\
  !*** ./src/app/mock-fridge.json ***!
  \**********************************/
/*! exports provided: user, products, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"user\":1000001,\"products\":[{\"ingredient\":2000001,\"amount\":100,\"expires\":\"11/28/2019\"},{\"ingredient\":2000002,\"amount\":500,\"expires\":\"11/28/2019\"},{\"ingredient\":2000005,\"amount\":3,\"expires\":\"11/28/2019\"}]}");

/***/ }),

/***/ "./src/app/mock-ingredient.json":
/*!**************************************!*\
  !*** ./src/app/mock-ingredient.json ***!
  \**************************************/
/*! exports provided: ingredient, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"ingredient\":[{\"ID\":2000001,\"name\":\"tomato\",\"unit\":\"g\",\"category\":\"vegetable\"},{\"ID\":2000002,\"name\":\"spaghetti\",\"unit\":\"g\",\"category\":\"noodle\"},{\"ID\":2000003,\"name\":\"water\",\"unit\":\"ml\",\"category\":\"fluid\"},{\"ID\":2000004,\"name\":\"milk\",\"unit\":\"ml\",\"category\":\"fluid\"},{\"ID\":2000005,\"name\":\"carrot\",\"unit\":\"pcs\",\"category\":\"vegetable\"},{\"ID\":2000006,\"name\":\"salt\",\"unit\":\"pcs\",\"category\":\"seasoning\"},{\"ID\":2000007,\"name\":\"pepper\",\"unit\":\"pcs\",\"category\":\"seasoning\"}]}");

/***/ }),

/***/ "./src/app/mock-rating.json":
/*!**********************************!*\
  !*** ./src/app/mock-rating.json ***!
  \**********************************/
/*! exports provided: 0, 1, 2, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"ID\":4000001,\"title\":\"Best meal ever!\",\"description\":\"it's just fantastic but -1 star due to lack of wrong ingredient list\",\"stars\":4,\"date\":\"datetime\",\"favorite\":true,\"recipe\":3000001,\"user\":1000001},{\"ID\":4000002,\"title\":\"Best meal ever!!!!!\",\"description\":\"it's just fantastic but -1 star due to lack of detailed ingredient list\",\"stars\":4,\"date\":\"datetime\",\"favorite\":true,\"recipe\":3000001,\"user\":1000001},{\"ID\":4000003,\"title\":\"Garbage\",\"description\":\"just dont\",\"stars\":1,\"date\":\"datetime\",\"favorite\":true,\"recipe\":3000001,\"user\":1000001}]");

/***/ }),

/***/ "./src/app/mock-recipe.json":
/*!**********************************!*\
  !*** ./src/app/mock-recipe.json ***!
  \**********************************/
/*! exports provided: recipes, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"recipes\":[{\"ID\":3000001,\"title\":\"Spaghetti Napoli\",\"description\":\"short description of dish\",\"ingredients\":[{\"ingredient\":2000001,\"amount\":100},{\"ingredient\":2000002,\"amount\":200},{\"ingredient\":2000006,\"amount\":20},{\"ingredient\":2000007,\"amount\":30}],\"preparation\":\"how you make it\",\"image\":\"base64 image string\",\"preparationtime\":20,\"difficulty\":2,\"origin\":\"italian\",\"tags\":[\"spaghetti\",\"noodle\",\"tomato\"],\"date\":\"28.11.2019\",\"user\":1000001},{\"ID\":3000002,\"title\":\"Spaghetti Bolognese\",\"description\":\"short description of dish\",\"ingredients\":[{\"ingredient\":2000001,\"amount\":100},{\"ingredient\":2000002,\"amount\":200},{\"ingredient\":2000005,\"amount\":1},{\"ingredient\":2000006,\"amount\":30},{\"ingredient\":2000007,\"amount\":40}],\"preparation\":\"how you make it\",\"image\":\"base64 image string\",\"preparationtime\":20,\"difficulty\":2,\"origin\":\"italian\",\"tags\":[\"spaghetti\",\"noodle\",\"tomato\"],\"date\":\"28.11.2019\",\"user\":1000001}]}");

/***/ }),

/***/ "./src/app/rating/rating.component.css":
/*!*********************************************!*\
  !*** ./src/app/rating/rating.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container{\r\n    border-bottom: 1px solid lightgray;\r\n    padding: 0.2em;\r\n}\r\n\r\n.gray{\r\n    color: gray;\r\n    font-weight: 100;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmF0aW5nL3JhdGluZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0NBQWtDO0lBQ2xDLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsZ0JBQWdCO0FBQ3BCIiwiZmlsZSI6InNyYy9hcHAvcmF0aW5nL3JhdGluZy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lcntcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XHJcbiAgICBwYWRkaW5nOiAwLjJlbTtcclxufVxyXG5cclxuLmdyYXl7XHJcbiAgICBjb2xvcjogZ3JheTtcclxuICAgIGZvbnQtd2VpZ2h0OiAxMDA7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/rating/rating.component.ts":
/*!********************************************!*\
  !*** ./src/app/rating/rating.component.ts ***!
  \********************************************/
/*! exports provided: RatingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatingComponent", function() { return RatingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _mock_rating_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../mock-rating.json */ "./src/app/mock-rating.json");
var _mock_rating_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../mock-rating.json */ "./src/app/mock-rating.json", 1);



let RatingComponent = class RatingComponent {
    constructor() {
        this.rating = _mock_rating_json__WEBPACK_IMPORTED_MODULE_2__[0];
    }
    ngOnInit() {
    }
};
RatingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-rating',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./rating.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/rating/rating.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./rating.component.css */ "./src/app/rating/rating.component.css")).default]
    })
], RatingComponent);



/***/ }),

/***/ "./src/app/recipe-container/recipe-container.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/recipe-container/recipe-container.component.css ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".headline{\r\n    background-color: lightgray;\r\n    padding: 0.2em;\r\n    border-radius: 5px;\r\n    text-align: center;\r\n    margin-bottom: 0.2em;\r\n}\r\n\r\n.list-group{\r\n    padding-top: 0.2em;\r\n}\r\n\r\n.dur-label{\r\n    width:35%;\r\n    display: inline-block;\r\n}\r\n\r\n#dur-selector{\r\n    width:65%;\r\n    display: inline-block;\r\n    padding-top: 0.7em;\r\n}\r\n\r\n.diff-label{\r\n    width:40%;\r\n    display: inline-block;\r\n}\r\n\r\n#diff-selector{\r\n    width:60%;\r\n    display: inline-block;\r\n    padding-top: 0.7em;\r\n}\r\n\r\n.custom-range::-webkit-slider-thumb {\r\n    background: #4CAF50; /* Green background */\r\n}\r\n\r\n.custom-range::-moz-range-thumb {\r\n    background: #4CAF50; /* Green background */\r\n}\r\n\r\napp-recipe{\r\n    cursor: pointer;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVjaXBlLWNvbnRhaW5lci9yZWNpcGUtY29udGFpbmVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSwyQkFBMkI7SUFDM0IsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsb0JBQW9CO0FBQ3hCOztBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksU0FBUztJQUNULHFCQUFxQjtBQUN6Qjs7QUFFQTtJQUNJLFNBQVM7SUFDVCxxQkFBcUI7SUFDckIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksU0FBUztJQUNULHFCQUFxQjtBQUN6Qjs7QUFFQTtJQUNJLFNBQVM7SUFDVCxxQkFBcUI7SUFDckIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksbUJBQW1CLEVBQUUscUJBQXFCO0FBQzlDOztBQUVBO0lBQ0ksbUJBQW1CLEVBQUUscUJBQXFCO0FBQzlDOztBQUVBO0lBQ0ksZUFBZTtBQUNuQiIsImZpbGUiOiJzcmMvYXBwL3JlY2lwZS1jb250YWluZXIvcmVjaXBlLWNvbnRhaW5lci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRsaW5le1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmF5O1xyXG4gICAgcGFkZGluZzogMC4yZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwLjJlbTtcclxufVxyXG5cclxuLmxpc3QtZ3JvdXB7XHJcbiAgICBwYWRkaW5nLXRvcDogMC4yZW07XHJcbn1cclxuXHJcbi5kdXItbGFiZWx7XHJcbiAgICB3aWR0aDozNSU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbiNkdXItc2VsZWN0b3J7XHJcbiAgICB3aWR0aDo2NSU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBwYWRkaW5nLXRvcDogMC43ZW07XHJcbn1cclxuXHJcbi5kaWZmLWxhYmVse1xyXG4gICAgd2lkdGg6NDAlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcblxyXG4jZGlmZi1zZWxlY3RvcntcclxuICAgIHdpZHRoOjYwJTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHBhZGRpbmctdG9wOiAwLjdlbTtcclxufVxyXG5cclxuLmN1c3RvbS1yYW5nZTo6LXdlYmtpdC1zbGlkZXItdGh1bWIge1xyXG4gICAgYmFja2dyb3VuZDogIzRDQUY1MDsgLyogR3JlZW4gYmFja2dyb3VuZCAqL1xyXG59XHJcbiAgXHJcbi5jdXN0b20tcmFuZ2U6Oi1tb3otcmFuZ2UtdGh1bWIge1xyXG4gICAgYmFja2dyb3VuZDogIzRDQUY1MDsgLyogR3JlZW4gYmFja2dyb3VuZCAqL1xyXG59XHJcblxyXG5hcHAtcmVjaXBle1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/recipe-container/recipe-container.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/recipe-container/recipe-container.component.ts ***!
  \****************************************************************/
/*! exports provided: RecipeContainerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecipeContainerComponent", function() { return RecipeContainerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _mock_recipe_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../mock-recipe.json */ "./src/app/mock-recipe.json");
var _mock_recipe_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../mock-recipe.json */ "./src/app/mock-recipe.json", 1);



let RecipeContainerComponent = class RecipeContainerComponent {
    constructor() {
        this.allRecipes = _mock_recipe_json__WEBPACK_IMPORTED_MODULE_2__.recipes;
        this.allRecipesCopies = this.allRecipes;
        this.searchRecipeTerm = "";
        this.maxDur = 180;
        this.selDur = this.maxDur;
        this.maxDiff = 3;
        this.selDiff = this.maxDiff;
        this.symDiff = ["", "beginner", "experienced", "expert"];
    }
    ngOnInit() {
    }
    searchRecipe() {
        let term = this.searchRecipeTerm;
        let dur = this.selDur;
        let diff = this.selDiff;
        this.allRecipes = this.allRecipesCopies.filter(function (obj) {
            return (obj.title.toLowerCase().indexOf(term) >= 0 || obj.origin.indexOf(term) >= 0) && (obj.preparationtime <= dur && obj.difficulty <= diff);
        });
    }
    filterDuration(dur) {
        this.selDur = dur;
        this.searchRecipe();
    }
    filterDifficulty(diff) {
        this.selDiff = diff;
        this.searchRecipe();
    }
};
RecipeContainerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-recipe-container',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./recipe-container.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/recipe-container/recipe-container.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./recipe-container.component.css */ "./src/app/recipe-container/recipe-container.component.css")).default]
    })
], RecipeContainerComponent);



/***/ }),

/***/ "./src/app/recipe-detail/recipe-detail.component.css":
/*!***********************************************************!*\
  !*** ./src/app/recipe-detail/recipe-detail.component.css ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".center{\r\n    margin: auto;\r\n    max-width: 1200px;\r\n}\r\n\r\n.row{\r\n    border: 1px solid lightgray;\r\n    border-radius: 5px;\r\n    margin: 0;\r\n    padding: 0;\r\n    margin-top: 0.2em;\r\n    width:100%;\r\n}\r\n\r\n.col-sm{\r\n    border: 1px solid lightgray;\r\n}\r\n\r\napp-ingredient-recipe:nth-child(odd)\r\n{\r\n    color: green ;\r\n}\r\n\r\n.rating-container{\r\n    border: 1px solid lightgray;\r\n    border-radius: 5px;\r\n    margin-top: 0.2em;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVjaXBlLWRldGFpbC9yZWNpcGUtZGV0YWlsLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZO0lBQ1osaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksMkJBQTJCO0lBQzNCLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsVUFBVTtJQUNWLGlCQUFpQjtJQUNqQixVQUFVO0FBQ2Q7O0FBRUE7SUFDSSwyQkFBMkI7QUFDL0I7O0FBRUE7O0lBRUksYUFBYTtBQUNqQjs7QUFHQTtJQUNJLDJCQUEyQjtJQUMzQixrQkFBa0I7SUFDbEIsaUJBQWlCO0FBQ3JCIiwiZmlsZSI6InNyYy9hcHAvcmVjaXBlLWRldGFpbC9yZWNpcGUtZGV0YWlsLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2VudGVye1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgbWF4LXdpZHRoOiAxMjAwcHg7XHJcbn1cclxuXHJcbi5yb3d7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgbWFyZ2luLXRvcDogMC4yZW07XHJcbiAgICB3aWR0aDoxMDAlO1xyXG59XHJcblxyXG4uY29sLXNte1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xyXG59XHJcblxyXG5hcHAtaW5ncmVkaWVudC1yZWNpcGU6bnRoLWNoaWxkKG9kZClcclxue1xyXG4gICAgY29sb3I6IGdyZWVuIDtcclxufVxyXG5cclxuXHJcbi5yYXRpbmctY29udGFpbmVye1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgbWFyZ2luLXRvcDogMC4yZW07XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/recipe-detail/recipe-detail.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/recipe-detail/recipe-detail.component.ts ***!
  \**********************************************************/
/*! exports provided: RecipeDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecipeDetailComponent", function() { return RecipeDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _mock_recipe_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../mock-recipe.json */ "./src/app/mock-recipe.json");
var _mock_recipe_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../mock-recipe.json */ "./src/app/mock-recipe.json", 1);
/* harmony import */ var _mock_ingredient_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../mock-ingredient.json */ "./src/app/mock-ingredient.json");
var _mock_ingredient_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../mock-ingredient.json */ "./src/app/mock-ingredient.json", 1);





let RecipeDetailComponent = class RecipeDetailComponent {
    constructor(route) {
        this.route = route;
        this.allIngredients = _mock_ingredient_json__WEBPACK_IMPORTED_MODULE_4__.ingredient;
    }
    ngOnInit() {
        const ID = +this.route.snapshot.paramMap.get("ID");
        /*
         /api/recipe/get/{id}
    
          this.recipe = ret;
         */
        this.recipe = _mock_recipe_json__WEBPACK_IMPORTED_MODULE_3__.recipes.find(obj => obj.ID === ID);
        this.allItems = this.recipe.ingredients.map((item, i) => Object.assign({}, item, this.allIngredients[i]));
    }
};
RecipeDetailComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
RecipeDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-recipe-detail',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./recipe-detail.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/recipe-detail/recipe-detail.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./recipe-detail.component.css */ "./src/app/recipe-detail/recipe-detail.component.css")).default]
    })
], RecipeDetailComponent);



/***/ }),

/***/ "./src/app/recipe/recipe.component.css":
/*!*********************************************!*\
  !*** ./src/app/recipe/recipe.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".recipe-comp{\r\n    border: 1px solid lightgray;\r\n    border-radius: 5px;\r\n    margin-top: 0.2em;\r\n}\r\n\r\n.recipe-dish-img{\r\n    display: inline-block;\r\n}\r\n\r\n.recipe-label{\r\n    width: 25%;\r\n    text-align: center;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVjaXBlL3JlY2lwZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksMkJBQTJCO0lBQzNCLGtCQUFrQjtJQUNsQixpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxxQkFBcUI7QUFDekI7O0FBRUE7SUFDSSxVQUFVO0lBQ1Ysa0JBQWtCO0FBQ3RCIiwiZmlsZSI6InNyYy9hcHAvcmVjaXBlL3JlY2lwZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnJlY2lwZS1jb21we1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgbWFyZ2luLXRvcDogMC4yZW07XHJcbn1cclxuXHJcbi5yZWNpcGUtZGlzaC1pbWd7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbi5yZWNpcGUtbGFiZWx7XHJcbiAgICB3aWR0aDogMjUlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/recipe/recipe.component.ts":
/*!********************************************!*\
  !*** ./src/app/recipe/recipe.component.ts ***!
  \********************************************/
/*! exports provided: RecipeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecipeComponent", function() { return RecipeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


// PLACEHOLDER IMG BASE64: data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEBLAEsAAD//gATQ3JlYXRlZCB3aXRoIEdJTVD/4gKwSUNDX1BST0ZJTEUAAQEAAAKgbGNtcwQwAABtbnRyUkdCIFhZWiAH4wAMAAIACQAAADdhY3NwTVNGVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9tYAAQAAAADTLWxjbXMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1kZXNjAAABIAAAAEBjcHJ0AAABYAAAADZ3dHB0AAABmAAAABRjaGFkAAABrAAAACxyWFlaAAAB2AAAABRiWFlaAAAB7AAAABRnWFlaAAACAAAAABRyVFJDAAACFAAAACBnVFJDAAACFAAAACBiVFJDAAACFAAAACBjaHJtAAACNAAAACRkbW5kAAACWAAAACRkbWRkAAACfAAAACRtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACQAAAAcAEcASQBNAFAAIABiAHUAaQBsAHQALQBpAG4AIABzAFIARwBCbWx1YwAAAAAAAAABAAAADGVuVVMAAAAaAAAAHABQAHUAYgBsAGkAYwAgAEQAbwBtAGEAaQBuAABYWVogAAAAAAAA9tYAAQAAAADTLXNmMzIAAAAAAAEMQgAABd7///MlAAAHkwAA/ZD///uh///9ogAAA9wAAMBuWFlaIAAAAAAAAG+gAAA49QAAA5BYWVogAAAAAAAAJJ8AAA+EAAC2xFhZWiAAAAAAAABilwAAt4cAABjZcGFyYQAAAAAAAwAAAAJmZgAA8qcAAA1ZAAAT0AAACltjaHJtAAAAAAADAAAAAKPXAABUfAAATM0AAJmaAAAmZwAAD1xtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAEcASQBNAFBtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEL/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAEAAQADAREAAhEBAxEB/8QAGgABAQEBAQEBAAAAAAAAAAAAAAMEAgEFCP/EABQBAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhADEAAAAf1SAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeA9AAAAAAAAAPD0AAAA5Ig0AAA4JngBQoAADgxG89AAABwSBY6AOCJ2UPQCZMqUABAzG46AAABMmCp2CRwXAOQdAEjwuAZzObjoAAAEiYLHZwTLAkDoHh4UPSYKgykTadgAAAiQBoKmY0HhIqegAEjs6IFz0yEjadgAAA8MQNhyeFCRQ9AAAJFTkmXMZM2FAAAACALmcueHBQAAAHJ4dkDQYyZsKAAAAEDw0GcucHR6AAAASKkDQZiBsKAAAAGcGgzlyZ2egAAAEyhA0HhhNRUAAAAznhpM5c5PDsAAAHhwUIGgGE0lQAAADOeGkidnRIqAAACRQ9IGgGE0FgAAADMeGo8IFzk5KAAA5OShI7OwYDSWAAAAMxyawQOzs4OSh6ATBQ8ImgAwGksAAAAZTk2AGYsdAmDw9Oz08IGg9APnmosAAAAZDw2AAznpYAAEyZoPQAfPNRYAAAAzHJrAAOCQAAKFAAAYDUVAAAAAAAAAAAAAAB4egAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/8QAJxAAAQIGAwABBAMAAAAAAAAAAQACAxAREjAyICExEyIzQHBBQkP/2gAIAQEAAQUC/ctVX8SoxHwL+vIuAXyq9yvcvlQeDzeaNxP1X8DybnWr6noQxxLAVRzE193GL4m64H6ybrJ77U1nCoVRwcyqa/hFk3XBE1kzVOdaGNndVWK0K0KxVLV7Jzapjqzi+pmuCLrcrlD1W7pb8yKIGsnihBrKJsma4CKgi1AXICgeaNYKNR7ODVyIqIZk/ZM1wxfIXiidmTPMDhUNNQvIifsma4YqhSP3E7UeYWSfsoglD1wxVCl/qn64m+p+0j7D1wxVCk7p6PYZrhZ4j3Ek72HrhiqF6og6BqEPpdgdNnbpH2HrhiqFsj2mdFOFQ08yaJok80DBQSPsLXDF9h7SiNTTcEW1V1ONyDZHpN+t3CFrhi+w9pkWEGs7FQqjlYvJeL7hApxha4YmzNuBZRCJzLwFQvQFOULzDEHcMd8S0OVhCucF8q+VXkq1zkGAYIev51B+5v/EABQRAQAAAAAAAAAAAAAAAAAAAKD/2gAIAQMBAT8BAB//xAAUEQEAAAAAAAAAAAAAAAAAAACg/9oACAECAQE/AQAf/8QAIxAAAQMEAgIDAQAAAAAAAAAAAQAQESAhMDECYUBRMnGAQf/aAAgBAQAGPwL8CFcFy+8GlpaWvDK4LmhT6FdsAwlcFzQftSad0dqDQGGLguaDyXsrmVpaVrK96I/tIxDpHt+n6rkPNI8CMPReKBiCLAPPvMWGII0HGWDTkCNBxlhmCLDOHOMVfeGHJc4xRD94JO/FkUXptdSdvPiSNUel8l8le9HXgimeKvgvrwZwWK0tNYK/4O//xAAoEAABAwIHAAIDAAMAAAAAAAABABExECEgMEFRYXGhkbFAcIHB4fD/2gAIAQEAAT8h/crLXF0xncfiAxZw+UbdS/yUwjFKlHY5OR5TP+qslNWbvG4KEC4yfEgv3K/qwAC/wnhQBN0ABAbBojdLUHCDwOErBzTwZe6L5KwAjhlO2Ai1C4nzgBeLJe1cEqPJlZUV68apzeq60X5KumRvQuMmCRTRGbhAgHFG/KbOhWHrLUHaLopwpDT41IVy2+6AbEU/lCALijEKYAjN09BHJijkwTUE8c2TRzTr9VGQZqDgFaOyk+bh6MBIQXask4hMJp7520Kq4aWoBgGVY3mkul+iHKhVGpCMr24CQ4YoGIcqHLpUXqguBE4ZJLB0M970tG1fVl00k6o8B2TQaXI0uGSbsMlC2HvuX3ZUkk6oDCEbh07ZO2NgxjFEvIpbdSmfesik7yoF9deyCulOqd0CtH+oF4wHTcVyT6UIA5QFzAwGVJ3lQ9KHAdQGLipDR+i2/C/6BXSKAALBqEgHKuwLAgAwwyd5X1YsY/BCNrDj5koHvYgAYYpe8o/AikToMU4L709rFYjNqZ1RgudPOMhiyFv6/EZNlE0gH9zf/9oADAMBAAIAAwAAABCSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSQSSSSSSSSSQSSSSSSCSSQSSSSSCSSSSSASSSCAAQSSSSSSSSASSQASACSSCCSSSSSCAASCAAASSSSSSSSSASQAACASSCSSSQSSASAAAACSSSSSSSSSCAAAAAQCCSSSSSCSAAAAAAQSSCSSSSQQCQAAAAASSSSSSSSSCAAAACACQCSSSSAQQAAAACQCQSSSSSSSACAAACACSSSSSSCSSCSAAQASQSSSSSSSCAACAACSCSSSSSQSQSAAACSSSCSSSSCSSSACACSSQCSSSSSSSSSASSSSQCSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSST/xAAUEQEAAAAAAAAAAAAAAAAAAACg/9oACAEDAQE/EAAf/8QAFBEBAAAAAAAAAAAAAAAAAAAAoP/aAAgBAgEBPxAAH//EACkQAQABAgQFBAMBAQAAAAAAAAERACEQMVFhMEFxobEggcHwkdHhQHD/2gAIAQEAAT8Q/wCyi2JyXzpNWAws5f5IAbQPCmGie1WVPffFXDp6p4GdC7QTDdTX9wa5juULNnRrVDSys/U7jNsUKhGEySoh1J4KihY+74puW/zTkah7eiWK/IZtMz9D+1zk98qFgBsYoJCSb1kUutJk7H/Kih6Lz6emLXSwU9F44Lj2vNZe39UXLefNOemMQGYuWlJnJeXzQQYoQk6LQzb8Cs8f5iNMZI2F9DsbOCnpuCoRqhTj8fisnu+aU9D5wDnlYKZZiuT5wUBVgOdLndMoqV6GhYo5w63pTkdCKWSjs2r9yY6lGEk1wKcy5Ou1My9TmYq1pLvh2HB+hs04gBy7/YoY2Nd8qc9Z84F1z+/fBQKsBzoJTI5FAABAcj1Ii8LRlLODW2zfrQZaScFK2Aw7Tgirk1GTo61DXq8ijNvHOreYbFSjnc4OItl2eKAABAcuAIBZodnAmeZTS81Jg+34w8rzwo7k1HcwOD5/LQQUoFci9MozJ9uDqRFutTdnF8Donz/uH0dMPO88J2t2snt+cHC0/U4P2UVtcBwhYZDjCzfHnBwks2euHl+XheZWb2/OAg/eWF/050pDtwrlw3nv84GwSOY0R2Qgpz1nzwvl+K7YwejsecN1iK3aEPtwQZZBNQTZtWFjaP3j3TzWT1eF2L8U7eBhebtW95gpG35jnwZqrt9igACwWKWCpMZcvvTHuHmnK2XC7ZpWN3kwF3JIpbXRkwCCYFx0aWeujXf1ztuuQZrRuodjTC0Hke1Q5IV3ripTVa+hscJ/jpR1LFBN4jzQgM+ZpgUEYchSllocmgMoTUxWCWjWHp5FMTdLTowUpAU4G9Y9Fy613DwcJWvq7Xl+MUkhyqA0rM0qVnU5mCCQkjyaly130n8goTMexqKnZltUYAbYOEgOdLflIGEBi5YfQ2OE5joKce949CCIkjyqXjT8wXKhEkZPVbB2ijD6R+qBhAer6mxwiEEohdGoG6j6u0BnStZDTL+VDzo5xRzEdGnkN96gdy7XaN/Co5je9aQWYxTSvNJ/kQ5g0AyDhIypqlBBBl/2X//Z
let RecipeComponent = class RecipeComponent {
    constructor() {
        this.symDiff = ["", "beginner", "experienced", "expert"];
        this.image = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEBLAEsAAD//gATQ3JlYXRlZCB3aXRoIEdJTVD/4gKwSUNDX1BST0ZJTEUAAQEAAAKgbGNtcwQwAABtbnRyUkdCIFhZWiAH4wAMAAIACQAAADdhY3NwTVNGVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9tYAAQAAAADTLWxjbXMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1kZXNjAAABIAAAAEBjcHJ0AAABYAAAADZ3dHB0AAABmAAAABRjaGFkAAABrAAAACxyWFlaAAAB2AAAABRiWFlaAAAB7AAAABRnWFlaAAACAAAAABRyVFJDAAACFAAAACBnVFJDAAACFAAAACBiVFJDAAACFAAAACBjaHJtAAACNAAAACRkbW5kAAACWAAAACRkbWRkAAACfAAAACRtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACQAAAAcAEcASQBNAFAAIABiAHUAaQBsAHQALQBpAG4AIABzAFIARwBCbWx1YwAAAAAAAAABAAAADGVuVVMAAAAaAAAAHABQAHUAYgBsAGkAYwAgAEQAbwBtAGEAaQBuAABYWVogAAAAAAAA9tYAAQAAAADTLXNmMzIAAAAAAAEMQgAABd7///MlAAAHkwAA/ZD///uh///9ogAAA9wAAMBuWFlaIAAAAAAAAG+gAAA49QAAA5BYWVogAAAAAAAAJJ8AAA+EAAC2xFhZWiAAAAAAAABilwAAt4cAABjZcGFyYQAAAAAAAwAAAAJmZgAA8qcAAA1ZAAAT0AAACltjaHJtAAAAAAADAAAAAKPXAABUfAAATM0AAJmaAAAmZwAAD1xtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAEcASQBNAFBtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEL/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAEAAQADAREAAhEBAxEB/8QAGgABAQEBAQEBAAAAAAAAAAAAAAMEAgEFCP/EABQBAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhADEAAAAf1SAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeA9AAAAAAAAAPD0AAAA5Ig0AAA4JngBQoAADgxG89AAABwSBY6AOCJ2UPQCZMqUABAzG46AAABMmCp2CRwXAOQdAEjwuAZzObjoAAAEiYLHZwTLAkDoHh4UPSYKgykTadgAAAiQBoKmY0HhIqegAEjs6IFz0yEjadgAAA8MQNhyeFCRQ9AAAJFTkmXMZM2FAAAACALmcueHBQAAAHJ4dkDQYyZsKAAAAEDw0GcucHR6AAAASKkDQZiBsKAAAAGcGgzlyZ2egAAAEyhA0HhhNRUAAAAznhpM5c5PDsAAAHhwUIGgGE0lQAAADOeGkidnRIqAAACRQ9IGgGE0FgAAADMeGo8IFzk5KAAA5OShI7OwYDSWAAAAMxyawQOzs4OSh6ATBQ8ImgAwGksAAAAZTk2AGYsdAmDw9Oz08IGg9APnmosAAAAZDw2AAznpYAAEyZoPQAfPNRYAAAAzHJrAAOCQAAKFAAAYDUVAAAAAAAAAAAAAAB4egAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/8QAJxAAAQIGAwABBAMAAAAAAAAAAQACAxAREjAyICExEyIzQHBBQkP/2gAIAQEAAQUC/ctVX8SoxHwL+vIuAXyq9yvcvlQeDzeaNxP1X8DybnWr6noQxxLAVRzE193GL4m64H6ybrJ77U1nCoVRwcyqa/hFk3XBE1kzVOdaGNndVWK0K0KxVLV7Jzapjqzi+pmuCLrcrlD1W7pb8yKIGsnihBrKJsma4CKgi1AXICgeaNYKNR7ODVyIqIZk/ZM1wxfIXiidmTPMDhUNNQvIifsma4YqhSP3E7UeYWSfsoglD1wxVCl/qn64m+p+0j7D1wxVCk7p6PYZrhZ4j3Ek72HrhiqF6og6BqEPpdgdNnbpH2HrhiqFsj2mdFOFQ08yaJok80DBQSPsLXDF9h7SiNTTcEW1V1ONyDZHpN+t3CFrhi+w9pkWEGs7FQqjlYvJeL7hApxha4YmzNuBZRCJzLwFQvQFOULzDEHcMd8S0OVhCucF8q+VXkq1zkGAYIev51B+5v/EABQRAQAAAAAAAAAAAAAAAAAAAKD/2gAIAQMBAT8BAB//xAAUEQEAAAAAAAAAAAAAAAAAAACg/9oACAECAQE/AQAf/8QAIxAAAQMEAgIDAQAAAAAAAAAAAQAQESAhMDECYUBRMnGAQf/aAAgBAQAGPwL8CFcFy+8GlpaWvDK4LmhT6FdsAwlcFzQftSad0dqDQGGLguaDyXsrmVpaVrK96I/tIxDpHt+n6rkPNI8CMPReKBiCLAPPvMWGII0HGWDTkCNBxlhmCLDOHOMVfeGHJc4xRD94JO/FkUXptdSdvPiSNUel8l8le9HXgimeKvgvrwZwWK0tNYK/4O//xAAoEAABAwIHAAIDAAMAAAAAAAABABExECEgMEFRYXGhkbFAcIHB4fD/2gAIAQEAAT8h/crLXF0xncfiAxZw+UbdS/yUwjFKlHY5OR5TP+qslNWbvG4KEC4yfEgv3K/qwAC/wnhQBN0ABAbBojdLUHCDwOErBzTwZe6L5KwAjhlO2Ai1C4nzgBeLJe1cEqPJlZUV68apzeq60X5KumRvQuMmCRTRGbhAgHFG/KbOhWHrLUHaLopwpDT41IVy2+6AbEU/lCALijEKYAjN09BHJijkwTUE8c2TRzTr9VGQZqDgFaOyk+bh6MBIQXask4hMJp7520Kq4aWoBgGVY3mkul+iHKhVGpCMr24CQ4YoGIcqHLpUXqguBE4ZJLB0M970tG1fVl00k6o8B2TQaXI0uGSbsMlC2HvuX3ZUkk6oDCEbh07ZO2NgxjFEvIpbdSmfesik7yoF9deyCulOqd0CtH+oF4wHTcVyT6UIA5QFzAwGVJ3lQ9KHAdQGLipDR+i2/C/6BXSKAALBqEgHKuwLAgAwwyd5X1YsY/BCNrDj5koHvYgAYYpe8o/AikToMU4L709rFYjNqZ1RgudPOMhiyFv6/EZNlE0gH9zf/9oADAMBAAIAAwAAABCSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSQSSSSSSSSSQSSSSSSCSSQSSSSSCSSSSSASSSCAAQSSSSSSSSASSQASACSSCCSSSSSCAASCAAASSSSSSSSSASQAACASSCSSSQSSASAAAACSSSSSSSSSCAAAAAQCCSSSSSCSAAAAAAQSSCSSSSQQCQAAAAASSSSSSSSSCAAAACACQCSSSSAQQAAAACQCQSSSSSSSACAAACACSSSSSSCSSCSAAQASQSSSSSSSCAACAACSCSSSSSQSQSAAACSSSCSSSSCSSSACACSSQCSSSSSSSSSASSSSQCSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSST/xAAUEQEAAAAAAAAAAAAAAAAAAACg/9oACAEDAQE/EAAf/8QAFBEBAAAAAAAAAAAAAAAAAAAAoP/aAAgBAgEBPxAAH//EACkQAQABAgQFBAMBAQAAAAAAAAERACEQMVFhMEFxobEggcHwkdHhQHD/2gAIAQEAAT8Q/wCyi2JyXzpNWAws5f5IAbQPCmGie1WVPffFXDp6p4GdC7QTDdTX9wa5juULNnRrVDSys/U7jNsUKhGEySoh1J4KihY+74puW/zTkah7eiWK/IZtMz9D+1zk98qFgBsYoJCSb1kUutJk7H/Kih6Lz6emLXSwU9F44Lj2vNZe39UXLefNOemMQGYuWlJnJeXzQQYoQk6LQzb8Cs8f5iNMZI2F9DsbOCnpuCoRqhTj8fisnu+aU9D5wDnlYKZZiuT5wUBVgOdLndMoqV6GhYo5w63pTkdCKWSjs2r9yY6lGEk1wKcy5Ou1My9TmYq1pLvh2HB+hs04gBy7/YoY2Nd8qc9Z84F1z+/fBQKsBzoJTI5FAABAcj1Ii8LRlLODW2zfrQZaScFK2Aw7Tgirk1GTo61DXq8ijNvHOreYbFSjnc4OItl2eKAABAcuAIBZodnAmeZTS81Jg+34w8rzwo7k1HcwOD5/LQQUoFci9MozJ9uDqRFutTdnF8Donz/uH0dMPO88J2t2snt+cHC0/U4P2UVtcBwhYZDjCzfHnBwks2euHl+XheZWb2/OAg/eWF/050pDtwrlw3nv84GwSOY0R2Qgpz1nzwvl+K7YwejsecN1iK3aEPtwQZZBNQTZtWFjaP3j3TzWT1eF2L8U7eBhebtW95gpG35jnwZqrt9igACwWKWCpMZcvvTHuHmnK2XC7ZpWN3kwF3JIpbXRkwCCYFx0aWeujXf1ztuuQZrRuodjTC0Hke1Q5IV3ripTVa+hscJ/jpR1LFBN4jzQgM+ZpgUEYchSllocmgMoTUxWCWjWHp5FMTdLTowUpAU4G9Y9Fy613DwcJWvq7Xl+MUkhyqA0rM0qVnU5mCCQkjyaly130n8goTMexqKnZltUYAbYOEgOdLflIGEBi5YfQ2OE5joKce949CCIkjyqXjT8wXKhEkZPVbB2ijD6R+qBhAer6mxwiEEohdGoG6j6u0BnStZDTL+VDzo5xRzEdGnkN96gdy7XaN/Co5je9aQWYxTSvNJ/kQ5g0AyDhIypqlBBBl/2X//Z";
    }
    ngOnInit() {
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], RecipeComponent.prototype, "recipe", void 0);
RecipeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-recipe',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./recipe.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/recipe/recipe.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./recipe.component.css */ "./src/app/recipe/recipe.component.css")).default]
    })
], RecipeComponent);



/***/ }),

/***/ "./src/app/register-form/register-form.component.css":
/*!***********************************************************!*\
  !*** ./src/app/register-form/register-form.component.css ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdGVyLWZvcm0vcmVnaXN0ZXItZm9ybS5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/register-form/register-form.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/register-form/register-form.component.ts ***!
  \**********************************************************/
/*! exports provided: RegisterFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterFormComponent", function() { return RegisterFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _http_requests_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../http-requests.service */ "./src/app/http-requests.service.ts");



let RegisterFormComponent = class RegisterFormComponent {
    constructor(httpService) {
        this.httpService = httpService;
        this.email = "";
        this.username = "";
        this.password = "";
        this.passwordrep = "";
    }
    ngOnInit() {
    }
    register() {
        if (this.password != this.passwordrep) {
            console.log("Passwords not matching");
        }
        else {
            this.httpService.register(this.email, this.username, this.password).subscribe();
            {
                res => {
                    console.log(res);
                    if (res.error) {
                        console.log(res.error.error);
                    }
                };
            }
        }
    }
};
RegisterFormComponent.ctorParameters = () => [
    { type: _http_requests_service__WEBPACK_IMPORTED_MODULE_2__["HttpRequestsService"] }
];
RegisterFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-register-form',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./register-form.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/register-form/register-form.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./register-form.component.css */ "./src/app/register-form/register-form.component.css")).default]
    })
], RegisterFormComponent);



/***/ }),

/***/ "./src/app/stars/stars.component.css":
/*!*******************************************!*\
  !*** ./src/app/stars/stars.component.css ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".star{\r\n    font-size: 1.1em;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3RhcnMvc3RhcnMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGdCQUFnQjtBQUNwQiIsImZpbGUiOiJzcmMvYXBwL3N0YXJzL3N0YXJzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3RhcntcclxuICAgIGZvbnQtc2l6ZTogMS4xZW07XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/stars/stars.component.ts":
/*!******************************************!*\
  !*** ./src/app/stars/stars.component.ts ***!
  \******************************************/
/*! exports provided: StarsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StarsComponent", function() { return StarsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let StarsComponent = class StarsComponent {
    constructor() {
        this.col = ["gray", "gray", "gray", "gray", "gray"];
    }
    ngOnInit() {
        let i = 0;
        while (i < Math.round(this.stars)) {
            this.col[i] = "orange";
            i++;
        }
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], StarsComponent.prototype, "stars", void 0);
StarsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-stars',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./stars.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/stars/stars.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./stars.component.css */ "./src/app/stars/stars.component.css")).default]
    })
], StarsComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\dufner\whatmeal-new\dev-frontend\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map