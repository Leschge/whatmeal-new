var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const app = require("./server");
const request = require('supertest');
const { tfk } = require("/server");
describe("GET / - a simple api endpoint", () => {
    it("Hello API Request", () => __awaiter(this, void 0, void 0, function* () {
        const result = yield request(app).get("/api/recipe/get/20");
        console.log(result);
        expect(result.text).toEqual("hello");
        expect(result.statusCode).toEqual(200);
    }));
});
//# sourceMappingURL=server.test.js.map