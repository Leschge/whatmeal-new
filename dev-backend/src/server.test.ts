const app = require("./server");
const request = require('supertest');
const {tfk} = require("/server")

    
describe("GET / - a simple api endpoint", () => {
  it("Hello API Request", async () => {
    const result = await request(app).get("/api/recipe/get/20");
    expect(result.text).toEqual("hello");
    expect(result.statusCode).toEqual(200);
  });
});