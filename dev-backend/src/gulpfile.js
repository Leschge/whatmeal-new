/* GULP is mainly used to make files smaller

TUT:
https://coder-coder.com/gulp-tutorial-beginners/
https://www.npmjs.com/package/gulp-terser

gulp-sass — compiles your Sass files into CSS
gulp-cssnano — minifies your CSS files
gulp-concat — concatenates (combines) multiple JS files into one large file
gulp-terser — minifies your JS files

COMMANDS:
npm install --global gulp-cli
npm install node-sass gulp-sass --save-dev
npm install gulp-cssnano --save-dev
npm install gulp-concat --save-dev 
npm install gulp-terser --save-dev

After setting up use:
gulp <task> 
to run defined gulp task
*/

var gulp = require('gulp');
var terser = require('gulp-terser') // terser used instead of gulps uglify because of ECMAScript 6
var ts = require('gulp-typescript');
//var cssnano = require('gulp-cssnano');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
//var uglify = require('gulp-uglify');

//default task
//gulp.task('default', ['sass', 'js', 'watch']);


// BACKEND COMMANDS //

// compile typescript to js
gulp.task("b-ts",function(){
    return gulp.src("server.ts")
        .pipe(ts({
            noImplicitAny: true
        }))
        .pipe(gulp.dest("../dist"));
})

// transform sass to css
gulp.task('b-sass',function(){
    return gulp.src('style.scss')
        .pipe(sass())
        //.pipe(cssnano())
        .pipe(gulp.dest('../dist'))
})

// transform js to smaller js
gulp.task('b-js',function(){
    return gulp.src(['server.js'])
        .pipe(concat('scripts.js'))
        .pipe(terser())
        .pipe(gulp.dest('../dist'));
})

// automation
gulp.task('watch',function(){
    gulp.watch('*.scss',['b-sass']);
    gulp.watch('*js',['b-js']);    
})
