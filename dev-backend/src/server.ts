import express from "express";
const app = express();
const port = 7000;

const mysql = require('mysql2');
var session = require('express-session');
var randomstring = require("randomstring");
const bodyParser = require('body-parser');
const sha256 = require('sha256');

require('dotenv').config()


app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use(express.static(__dirname+"./../../dist/frontend"));
app.use(express.json());
app.use(bodyParser.urlencoded({
  extended: true,
}));

// give sessionID
app.use(session({
  genid: function(req:any) {
    let sid = randomstring.generate(36)
    return sid
  },
  secret: randomstring.generate(8)
}));

const databasename = process.env.DB_NAME;
const connectionPool = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  port: process.env.DB_PORT,
  database: databasename,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
  multipleStatements: true
});

// testConnection checks the database connection
// to database (using asyncQuery) and running SELECT 1
// Exits whenever the connection to database could not be established
async function testConnection() {
  try {
      await asyncQuery('SELECT 1', []);
      console.log('Successfully connected to database');
      return "success"
  } catch (err) {
      console.log('Cannot connect to MySQL Database: ' + err);
      return "fail"
      process.exit();
  }
}

//User functions -------------------------------------------------------------------------------------------------------------------------------------------

// returs userID if client is logged in user
app.get("/api/user/getMyUserID", async (req:any,res)=>{

  const uid = await getUserFromSession(req.sessionID);

if(isNull(uid)){
  res.json({status: -1, userID : -1, message : 'you are not logged in'});
}else{
  res.json({userID : uid})
}
})


// register(...<parameter>...) : boolean
// mandatory:  username, email, password,
// optional:   avatar, about, experience, birthday 
app.post("/api/user/register", async (req:any, res:any) =>{
  if(isNull(req.body.username)){
    return res.status(400).json({status: -1, error: 'missing username'});

  }else if(isNull(req.body.email)){
    return res.status(400).json({status: -1, error: 'missing email'});

  }else if(isNull(req.body.password)){
    return res.status(400).json({status: -1, error: 'missing password'});
  }

  const salt = randomstring.generate(64);
  const pwHash = sha256(salt+req.body.password);  //hash salt with password

  const sql = createInsertQuery('user',['mail','username','pwhash','salt','avatar','about','experience','birthday'],[req.body.email,req.body.username,pwHash,salt,req.body.avatar,req.body.about,req.body.experience,req.body.birthday]);
  try {
    const result = await asyncQuery(sql, []);
     
    return res.json(extractSuccessFromDbResult(result));
 } catch (err) {
     return res.json({status: -1, message: 'mail or username already in use', error: err});
 }
})

// login(username,password) : boolean
// uses sessionID, that user already has
app.post("/api/user/login", async (req:any, res:any) =>{
  //already logged in?
  if(!isNull(await getUserFromSession(req.sessionID))){
    const uid = await getUserFromSession(req.sessionID);
    return res.json({status: -1, error: 'already logged in! userID = '+uid});
  }
  //username available?
  if(isNull(req.body.username)){
    if(req.body.username = "")
    {
      return res.status(400).json({status: -1, error: 'missing username'});
    }

  // password available?
  }else if(isNull(req.body.password)){
    if(req.body.password = "")
    {
      return res.status(400).json({status: -1, error: 'missing password'});
    }
  }

  //delete all old sessions of this user in the database
  //get userID
  let sql = `SELECT \`userID\` FROM \`${databasename}\`.\`user\` WHERE \`username\`= '${req.body.username}';`;
  let result:any = await asyncQuery(sql,[]);

  let userID = 0;

  if(!isNull(result[0])){
    userID = result[0].userID;
  }else{
    return res.json({status: -1, message:'user not found', userID : -1})
  }
    
  
 
  // delete old sessions of this user
  result = await asyncQuery(`DELETE FROM ${databasename}.session WHERE  userID=?;`,[userID]);

  //get salt and pwhash from database
  sql = `SELECT \`salt\`, \`pwhash\` FROM \`${databasename}\`.\`user\` WHERE \`username\`= '${req.body.username}';`;
  result = await asyncQuery(sql,[]);
  const salt = result[0].salt;
  const pwhash = result[0].pwhash;

  if(sha256(salt+req.body.password) === pwhash){
    // Login successful -> create session
    

    sql = createInsertQuery('session',['SessionID','userID'],[req.sessionID,userID]);
    const rows =  await asyncQuery(sql,[]);    

  }else{

    return res.json({status: -1, message: 'username or password is wrong'});
  }

  
  const uid = await getUserFromSession(req.sessionID);

  if(isNull(uid)){
    return res.json({status: -1, userID : -1})
  }else{
    return res.json({userID : uid})
  }

})


// logout(): bool
// deletes session
app.post("/api/user/logout", async (req:any,res:any) =>{
  //logged in?
  if(isNull(await getUserFromSession(req.sessionID))){
    return res.json({status: -1, error: 'you cannot logout, because you are not logged in'});
  }

  try{
  //delete session
  const result = await asyncQuery(`DELETE FROM ${databasename}.session WHERE  SessionID=?;`,[req.sessionID])
  
  return res.json(extractSuccessFromDbResult(result));
 } catch (err) {
     return res.json({status: -1, message: 'fail', error: err});
 }

})

// Get User (id) : user
app.get("/api/user/get/:id", async (req, res) => {
  const userID = req.params.id;
  if (isNull(userID)) {
      return res.status(400).json({status: -1, error: 'URI missing userID'});
  }
  try {
    const rows = await asyncQuery("SELECT `userID`, `mail`, `username`, `avatar`, `about`, `experience`, `birthday` FROM user WHERE userID = ?", [userID]);
    return res.json(rows);
} catch (err) {
    return res.json({status: -1, message: 'fail', error: err});
}
});

// Logged can modify his data
app.post("/api/user/modify", async (req:any,res:any) =>{
  if(isNull(await getUserFromSession(req.sessionID))){
    return res.json({status : -1, message : 'not logged in'})
  }

  let colNames = ["mail","username","avatar","about","experience","birthday"] ;
  let entrys = [req.body.mail, req.body.username, req.body.avatar, req.body.about, req.body.experience, req.body.birthday];

  const sql = createUpdateQuery("user", colNames, entrys, `WHERE \`userID\`=${await getUserFromSession(req.sessionID)}`)
  
try{
  let result = await asyncQuery(sql, [])
  return res.json(extractSuccessFromDbResult(result));
} catch (err) {
    return res.json({status: -1, message: 'fail', error: err});
}

})

// ingredient functions -------------------------------------------------------------------------------------------------------------------------------------------

//Add ingredient to database
app.post("/api/ingredient/add", async (req,res)=>{
  if(isNull(req.body.name)){
    return res.json({status : -1, message : 'name not provided'})
  }
  if(isNull(req.body.unit)){
    return res.json({status : -1, message : 'unit not provided'})
  }

  // already in database?
  let result:any = await asyncQuery(`SELECT * FROM ${databasename}.ingredient WHERE name = ?`,[req.body.name])
  if(result.length > 0){

    return res.json({status: 1, message: req.body.name+' already in database', res: result})
  }

  try{
  result = await asyncQuery(`INSERT INTO ${databasename}.ingredient (name, unit) VALUES (?, ?);`,[req.body.name,req.body.unit,req.body.category])
  return res.json(extractSuccessFromDbResult(result));
} catch (err) {
    return res.json({status: -1, message: 'fail', error: err});
}
})

//Get ingredient by name or id
app.get("/api/ingredient/get/:nameOrId", async (req,res)=>{
  const result:any = await asyncQuery(`SELECT * FROM ${databasename}.ingredient WHERE name=? OR ingredientID=?;`,[req.params.nameOrId,req.params.nameOrId])
  return res.json(result)
})

//Get all ingredients
app.get("/api/ingredient/getAll", async (req,res)=>{
  const result:any = await asyncQuery(`SELECT * FROM ${databasename}.ingredient;`,[])
  return res.json(result);
})


// fridge functions  -------------------------------------------------------------------------------------------------------------------------------------------

// has to be logged in
// required: ingredientID, amount
// optional: expirationDate
app.post("/api/fridge/add", async (req:any,res:any)=>{

  const uid = await getUserFromSession(req.sessionID);

 if(isNull(uid)){
  return res.json({status : -1, message : 'not logged in'})

 }else if(isNull(req.body.ingredientID)){
  return res.json({status : -1, message : 'ingredientID not provided'})

  }else if(isNull(req.body.amount)){
    return res.json({status : -1, message : 'amount not provided'})
  }else{

// already there?
let result:any = await asyncQuery(`SELECT * FROM ${databasename}.fridge WHERE userID=? AND ingredientID=?`,[uid,req.body.ingredientID])
if(isNull(result[0])){

  
  let  sql = createInsertQuery('fridge',['userID','ingredientID','amount','expirationdate'],[uid,req.body.ingredientID,req.body.amount,req.body.expirationDate])
try{
  result = await asyncQuery(sql,[])
  return res.json(extractSuccessFromDbResult(result));
} catch (err) {
    return res.json({status: -1, message: 'fail', error: err});
}
}

const newAmount:number = Number(result[0].amount) + Number(req.body.amount);
try{
result = await asyncQuery(`UPDATE ${databasename}.fridge SET amount=? WHERE userID=? AND ingredientID=?`,[newAmount, uid, req.body.ingredientID])
return res.json(extractSuccessFromDbResult(result));
} catch (err) {
    return res.json({status: -1, message: 'fail', error: err});
}
}
})

// shows all entrys in fridge
app.get("/api/fridge/get/:userID", async (req:any,res)=>{
  let uid = req.params.userID;
  if(isNull(uid)){
    uid = await getUserFromSession(req.sessionID)
  }
  if(isNull(uid)){
    return res.json({status : -1, message : 'no valid userID provided'})
  }

  let result = await asyncQuery(`SELECT ingredient.ingredientID, ingredient.name, fridge.amount, ingredient.unit, fridge.expirationdate FROM ${databasename}.fridge, ${databasename}.ingredient WHERE fridge.ingredientID = ingredient.ingredientID AND fridge.userID=?`,[uid])

  return res.json(result)
 })

 // deletes ingredient from fridge from logged user (if logged user has given ing. uin fridge)
 app.post("/api/fridge/del/:ingredientID", async (req:any,res)=>{
  let uid = await getUserFromSession(req.sessionID)
  if(isNull(uid)){
    return res.json({status : -1, message : 'not logged in'})
  }

  try{
  let result = await asyncQuery(`DELETE FROM ${databasename}.fridge WHERE userID=? AND ingredientID=?`,[uid,req.params.ingredientID]);
  return res.json(extractSuccessFromDbResult(result));
  } catch (err) {
    return res.json({status: -1, message: 'fail', error: err});
}
 })


 // reduces stock of given ingredient in logged's fridge of given amount (deletes if zero)
 app.get("/api/fridge/reduce/:ingredientID/:amount", async (req:any,res)=>{
  let uid = await getUserFromSession(req.sessionID)
  if(isNull(uid)){
    return res.json({status : -1, message : 'not logged in'})
  }

  let result:any = await asyncQuery(`SELECT amount FROM ${databasename}.fridge WHERE userID=? AND ingredientID=?`,[uid,req.params.ingredientID])

  if(isNull(result[0])){
    return res.json({status : -1, message : `Ingredient with ID = ${req.params.ingredientID} is not in your fridge`})
  }

  if(result[0].amount - req.params.amount <= 0){
    //delete
  try{
    let result = await asyncQuery(`DELETE FROM ${databasename}.fridge WHERE userID=? AND ingredientID=?`,[uid,req.params.ingredientID]);
    return res.json(extractSuccessFromDbResult(result));
    } catch (err) {
      return res.json({status: -1, message: 'fail', error: err});
    }

  }else{
    //reduce
    const newAmount = result[0].amount - req.params.amount;
    
    try{
        const sql = createUpdateQuery('fridge',['amount'],[newAmount],`WHERE userID=${uid} AND ingredientID=${req.params.ingredientID}`)
        
        let result = await asyncQuery(sql,[])

      return res.json(extractSuccessFromDbResult(result));
      } catch (err) {
        return res.json({status: -1, message: 'fail', error: err});
      }

  }

 })


 
// deletes all from fridge from loigged user
app.post("/api/fridge/clear", async (req:any,res)=>{
  let uid = await getUserFromSession(req.sessionID)
  if(isNull(uid)){
    return res.json({status : -1, message : 'not logged in'})
  }

  try{
    let result = await asyncQuery(`DELETE FROM ${databasename}.fridge WHERE userID=?`,[uid]);
    return res.json(extractSuccessFromDbResult(result));
  } catch (err) {
    return res.json({status: -1, message: 'fail', error: err});
}
})


// recipe functions  -------------------------------------------------------------------------------------------------------------------------------------------

//  Recipe:

/*{
	"title":
		"Cheeseburger",
	"description":
		"bread with cheese and meat",
	"ingredients": 
		[
			{
				"ingredient": 2,  //ingid
				"amount": 2
			},
			{
				"ingredient": 22,  //ingid
				"amount": 80
      }
		],
	"portions":
	    1,
	"preparation": 
		"grill the meat and put it with the cheese on the bread",
	"image":
		"base64 image string",
	"preparation_time":
		20,
	"difficulty":
		10,
	"origin":
		"america",
	"tags":
		[
      1,
      3
		],
}*/
app.post("/api/recipe/addOrUpdate", async (req:any,res)=>{                                                               

const recipe = req.body

  const uid = await getUserFromSession(req.sessionID)
  if(isNull(uid)){
    return res.json({status : -1, message : 'not logged in'})
  }

  const title = recipe.title;
  const description = recipe.description;
  const portions = recipe.portions;
  const preparation = recipe.preparation;
  const image = recipe.image;
  const preparation_time = recipe.preparation_time;
  const difficulty = recipe.difficulty;
  const origin = recipe.origin;

  let date_ob = new Date();
  let day = ("0" + date_ob.getDate()).slice(-2);
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
  let year = date_ob.getFullYear();

  const date = (year + "-" + month + "-" + day)

  const ingredients: any[] = recipe.ingredients;
  const tags: any[] = recipe.tags;


  //Check if all ingredients exist
  let sql = `SELECT ingredientID FROM ${databasename}.ingredient WHERE `;
  for(let i=0; i<ingredients.length; i++){
    sql = sql+`ingredientID =${ingredients[i].ingredient} OR `
  }

  sql= sql.substring(0,sql.length-3);


  try{
    let result:any = await asyncQuery(sql,[]);
    
    if(result.length < ingredients.length){
      return res.json({status: -1, message: 'your recipe includes ingredients that are not in the database', result : result, ingredients: ingredients});
    }
    
  } catch (err) {
      return res.json({status: -1, message: 'error at checking for ingredients', error: err});
  }
  

  //insert tags in database, if they do not already exist
  for(let i=0; i<tags.length; i++){
    let sql = createInsertQuery('tag',['name'],[tags[i]]);
    sql = 'REPLACE'+sql.substring(6,sql.length)

    try{
      let result = await asyncQuery(sql,[]);
    } catch (err) {
        return res.json({status: -1, message: 'error at tags!', error: err});
    }
  }

  // insert recipe
  sql = createInsertQuery('recipe',['userID','title','description','portions','preparation','image','preptime','difficulty','origin','date'],[uid,title,description,portions,preparation,image,preparation_time,difficulty,origin,date])
  sql = 'REPLACE'+sql.substring(6,sql.length)

  try{
    let result = await asyncQuery(sql,[]);
  } catch (err) {
      return res.json({status: -1, message: 'error at inserting or updating', error: err});
  }

  let result:any = await asyncQuery(`SELECT recipeID FROM ${databasename}.recipe WHERE title=? AND userID=? AND description=? AND date=?`,[title,uid,description,date])
  const recID = result[0].recipeID;

  // insert tags
  result = await asyncQuery(`DELETE FROM ${databasename}.has WHERE recipeID=?`,[recID])
  for(let i=0; i<tags.length; i++){
    sql = createInsertQuery('has',['recipeID','tagID'],[recID,tags[i]])
    try{
      let result = await asyncQuery(sql,[]);
    } catch (err) {
        return res.json({status: -1, message: 'error at inserting tags',sql: sql, error: err});
    }
  }

  //insert ingredients
  result = await asyncQuery(`DELETE FROM ${databasename}.needs WHERE recipeID=?`,[recID])

  for(let i=0; i<ingredients.length; i++){
    sql = createInsertQuery('needs',['recipeID','ingredientID','amount'],[recID,ingredients[i].ingredient,ingredients[i].amount])
    try{
      let result = await asyncQuery(sql,[]);      
    } catch (err) {
        return res.json({status: -1, message: 'error at inserting ingredients', sql: sql ,error: err});
    }
  }


  return res.json({status : 1, message: 'success: recipe added or updated!'})

})

// returns recipe
app.get("/api/recipe/get/:recID", async (req,res)=>{
  const result:any = await asyncQuery(`SELECT * FROM ${databasename}.recipe WHERE recipeID=?`,[req.params.recID])
  
  if(isNull(result[0])){
    return res.json({status : -1, message : `recipe with recId = ${req.params.recID} is not in the database`})
  }
  return res.json(result)
})

// returns average rating
app.get("/api/recipe/getRating/:recID", async (req,res)=>{
  
  try{
    let result:any = await asyncQuery(`SELECT SUM(stars)/COUNT(stars) AS result FROM ${databasename}.rating WHERE recipeID = ?`,[req.params.recID])

    if(isNull(result[0].result)){
    return res.json({status: -1, message: 'given rec ID has no rating'});
    }

    return res.json(result);
  } catch (err) {
    return res.json({status: -1, message: 'fail', error: err});
}
})

//Returns ingredients for recipe
app.get("/api/recipe/getIngredients/:recID", async (req: any,res)=>{
  let result = await asyncQuery(`SELECT ingredient.ingredientID, ingredient.name, needs.amount, ingredient.unit FROM ${databasename}.ingredient, ${databasename}.needs WHERE ingredient.ingredientID=needs.ingredientID AND needs.recipeID=?`,[req.params.recID])
  return res.json(result)
})

//Returns tags from recipe
app.get("/api/recipe/getTags/:recID", async (req: any,res)=>{
  const result = await asyncQuery(`SELECT tag.name FROM ${databasename}.has, ${databasename}.tag WHERE has.tagID = tag.tagID AND has.recipeID=?`,[req.params.recID])

  return res.json(result)
})

//Returns all recipes from user
app.get("/api/recipe/fromUser/:userID", async (req,res)=>{

  if(isNull(req.params.userID)){
    return res.json({status : -1, message : 'userID not provided'})
  }

  const result = await asyncQuery(`SELECT * FROM ${databasename}.recipe WHERE userID=?`,[req.params.userID])

  return res.json(result)
})

//Returns all recipes
app.get("/api/recipe/getAll", async (req,res)=>{
  const result = await asyncQuery(`SELECT * FROM ${databasename}.recipe`,[])
  return res.json(result)
})

// logged can only delete his recipes
 app.post("/api/recipe/del/:recID", async (req: any,res)=>{
  const uid = await getUserFromSession(req.sessionID)
  if(isNull(uid)){
    return res.json({status : -1, message : 'not logged in'})
  }
  if(isNull(req.params.recID)){
    return res.json({status : -1, message : 'recipeId not provided'})
  }

  const sql = `DELETE FROM ${databasename}.recipe WHERE recipeID=${req.params.recID} AND userID=${uid}`
  
  try{
  const result = await asyncQuery(sql,[])
  return res.json(extractSuccessFromDbResult(result));
} catch (err) {
    return res.json({status: -1, message: 'fail', error: err});
}
 })


 app.get("/api/recipe/getByTag/:tag", async (req,res)=>{
   const result = await asyncQuery(`SELECT recipe.* FROM ${databasename}.recipe, ${databasename}.has, ${databasename}.tag WHERE recipe.recipeID=has.recipeID AND has.tagID=tag.tagID AND tag.name=?`,[req.params.tag])
   return res.json(result)
 })

 // Tag functions ----------------------------------------------------------------------------------------------------------------------------------

 //Returns all tags
 app.get("/api/tags/getAll", async (req,res)=>{
  const result = await asyncQuery(`SELECT * FROM ${databasename}.tag`,[])
  return res.json(result)
 })

 // Returns tag of given name or ID
 app.get("/api/tags/getTagByNameOrID/:tag", async(req,res)=>{
  const result = await asyncQuery(`SELECT * FROM ${databasename}.tag WHERE name=? OR tagID=?`,[req.params.tag, req.params.tag])
  return res.json(result)
 })

 //Adds the tag, returns ID
 app.post("/api/tags/add/:tagName", async(req,res)=>{
   let sql = createInsertQuery('tag',['name'],[req.params.tagName])
   sql = 'REPLACE'+sql.substring(6,sql.length)

   try{
    let result = await asyncQuery(sql,[]);
  } catch (err) {
      return res.json({status: -1, message: 'error', error: err});
  }

  const result = await asyncQuery(`SELECT * FROM ${databasename}.tag WHERE name=?`,[req.params.tagName])
  return res.json(result)

 })


// rating functions  -------------------------------------------------------------------------------------------------------------------------------------------


//required: recipeID, stars
//optional: favourite (1 or 0)
//User has to be logged in
//if rating already exists it gets updated
app.post("/api/rating/add", async (req:any,res)=>{

  let uid = await getUserFromSession(req.sessionID)
  if(isNull(uid)){
    return res.json({status : -1, message : 'not logged in'})
  }
  if(isNull(req.body.recipeID)){
    return res.json({status : -1, message : 'recipeID not provided'})
  }
  if(isNull(req.body.stars)){
    return res.json({status : -1, message : 'stars not provided'})
  }

  let date_ob = new Date();
  let day = ("0" + date_ob.getDate()).slice(-2);
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
  let year = date_ob.getFullYear();

  let date = (year + "-" + month + "-" + day)

  let favourite = 0;
  if(req.body.favourite == 1 || req.body.favourite == true || req.body.favourite == "true" || req.body.favourite == "TRUE"){
    favourite = 1;
  }

  //rating already exists?


  let sql = createInsertQuery('rating',['userID','recipeID','stars','date','favourite'],[uid,req.body.recipeID,req.body.stars,date,favourite]);

  sql = 'REPLACE'+sql.substring(6,sql.length)

  try{
  const result = await asyncQuery(sql,[]);
  return res.json(extractSuccessFromDbResult(result));
} catch (err) {
    return res.json({status: -1, message: 'fail', error: err});
}
})

//returns all ratings from given recipe
app.get("/api/rating/getAll/:recID", async (req,res)=>{

  if(isNull(req.params.recID)){
    return res.json({status : -1, message : 'recID not provided'})
  }

  const result = await asyncQuery(`SELECT * FROM ${databasename}.rating WHERE recipeID=?`,[req.params.recID])

  return res.json(result)
})

//returns all ratings from given user
app.get("/api/rating/getUser/:userID", async (req,res)=>{
  if(isNull(req.params.userID)){
    return res.json({status : -1, message : 'userID not provided'})
  }

  const result = await asyncQuery(`SELECT * FROM ${databasename}.rating WHERE userID=?`,[req.params.userID])

  return res.json(result)
})



// helping functions  -------------------------------------------------------------------------------------------------------------------------------------------

function extractSuccessFromDbResult (result:any){

   let res: any;
  try{
    if(result.affectedRows >=1){
      res = {status: 1, message: 'success', databaseOutput: result}
    }else{
      res = {status: -1, message: 'affectedRows = 0   -> nothing changed in database'}
    }
  }catch{
    res = {status: -1, message: 'fail'}
  }

  return res;
}

// returns true if value is Null or something similar
function isNull (value:any){
  if (value === undefined || value === '' || value === null || value === 'NULL') {
    return true;
  }else{
    return false;
  }
}

// returns userID if user is logged in
async function getUserFromSession(sessionID:any){
  let result:any = await asyncQuery(`SELECT \`userID\` FROM \`${databasename}\`.\`session\` WHERE \`SessionID\`=\'${sessionID}\';`,[]);

  if(isNull(result[0])){
    return null
  }else{
    return result[0].userID
  }
}

// colNames[] have to be of equal length as entrys[]
function createInsertQuery(table:any, colNames: any[], entrys :any[]){

  if(colNames.length != entrys.length){
    return "ERROR: colNames[] have to be of equal length as entrys[] and is not!"
  }

  for(let i=0; i<entrys.length; i++){
    if (isNull(entrys[i])) {
      entrys[i] = '';
      colNames[i] = '';
    }else{
      entrys[i] = "'"+entrys[i]+"', ";
      colNames[i] = `\`${colNames[i]}\`, `;
    }
  }

  let sqlfirstpart = `INSERT INTO \`${databasename}\`.\`${table}\` (`+colNames.join("");
  let sqlsecondpart = `) VALUES (`+entrys.join("");

  return sqlfirstpart.substring(0,sqlfirstpart.length -2)+sqlsecondpart.substring(0,sqlsecondpart.length -2)+");";
}

// condition = "WHERE `col` = 'value';"   // number without ''
function createUpdateQuery(table:any, colNames:any[], entrys:any[], condition:string){
  if(colNames.length != entrys.length){
    return "ERROR: colNames[] have to be of equal length as entrys[] and is not!"
  }

  let sqlMain = '';

  for(let i=0; i<entrys.length; i++){
    if(!isNull(entrys[i])){
     sqlMain += `\`${colNames[i]}\` = '${entrys[i]}', `
    }
  }
  
  let sqlStart =`UPDATE \`${databasename}\`.\`${table}\` SET `;

  const sql = sqlStart+ sqlMain.substring(0, sqlMain.length -2) +" "+condition;

  return sql;
}

//Query with placeholders as ? sign
// Bsp.: sql =  'SELECT * FROM `table` WHERE `name` = ? AND `age` > ?'
//      args = ['Page', 45]
async function asyncQuery(sql:any, args:any) {
  return new Promise((resolve, reject) => {
      connectionPool.getConnection(function(err:any, connection:any) {
          if (err) {
              if (connection !== undefined) {
                  connection.release();
              }
              return reject(err);
          }
          connection.query(sql, args, (err:any, rows:any) => {
              // console.log(mysql.format(sql, args));
              connection.release();
              if (err) {
                  return reject(err);
              }
              resolve(rows);
          });
      });
  });
}

module.exports = app.listen(port, () => console.log(`LOG: server listens on port ${port}`));
testConnection();

// ----------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------
//  -------------------------------------------------------------------------------
// ----  Testing -----------------------------------------------------------------




app.get("/api/user/register", (req,res)=>{
  res.send(`
    <h1>Registrieren Test<h1>
    <form method='post' action='/api/user/register'>
      <input name='username' placeholder='username' required />
      <input type='email' name='email' placeholder='email' required />
      <input type='password' name='password' placeholder='password' required />
      <input type='submit' />
    </form>
    <a>
  `)
})

app.get("/api/user/login", (req,res)=>{
  res.send(`
    <h1>Login Test<h1>
    <form method='post' action='/api/user/login'>
      <input name='username' placeholder='username' required />
      <input type='password' name='password' placeholder='password' required />
      <input type='submit' />
    </form>
    <a>
  `)
})

app.get("/api/user/logout", (req,res)=>{
  res.send(`
    <h1>Logout Test<h1>
    <form method='post' action='/api/user/logout'>
      <input type='submit' />
    </form>
    <a>
  `)
})

app.get("/api/ingredient/add", async (req,res)=>{
  res.send(`
    <h1>Ingredient Add<h1>
    <form method='post' action='/api/ingredient/add'>
    <input name='name' placeholder='name'  />
    <input name='unit' placeholder='unit'  />
      <input type='submit' />
    </form>
    <a>
  `)

})

app.get("/api/user/modify", async (req,res)=>{
  res.send(`
    <h1>Modyfy User<h1>
    <form method='post' action='/api/user/modify'>
    <input name='mail' placeholder='mail'  />
    <input name='username' placeholder='username'  />
    <input name='avatar' placeholder='avatar'  />
    <input name='about' placeholder='about'  />
    <input name='experience' placeholder='experience'  />
    <input name='birthday' placeholder='birthday'  />
    <input type='submit' />
    </form>
    <a>
  `)

})

app.get("/api/ingredient/add", async (req,res)=>{
  res.send(`
    <h1>add ingredient<h1>
    <form method='post' action='/api/ingredient/add'>
    <input name='name' placeholder='name'  />
    <input name='unit' placeholder='unit'  />
    <input type='submit' />
    </form>
    <a>
  `)
})

app.get("/api/fridge/add", async (req,res)=>{
  res.send(`
    <h1>add to fridge<h1>
    <form method='post' action='/api/fridge/add'>
    <input name='ingredientID' placeholder='ingredientID'  />
    <input name='amount' placeholder='amount'  />
    <input name='expirationDate' placeholder='expirationDate'  />
    <input type='submit' />
    </form>
    <a>
  `)

})

app.get("/api/fridge/clear", async (req,res)=>{
  res.send(`
    <h1>clear fridge<h1>
    <form method='post' action='/api/fridge/clear'>
    <input type='submit' />
    </form>
    <a>
  `)

})

app.get("/api/rating/add", async (req,res)=>{
  res.send(`
    <h1>add Rating for Recipe<h1>
    <form method='post' action='/api/rating/add'>
    <input name='recipeID' placeholder='recipeID'  />
    <input name='stars' placeholder='stars'  />
    <input name='favourite' placeholder='favourite'  />
    <input type='submit' />
    </form>
    <a>
  `)

})

app.get("/api/recipe/add",async (req,res)=>{
  res.send(`
    <h1>add Recipe<h1>
    <form method='post' action='/api/rating/add'> 
    <input type="text" name='ingredients' placeholder='ingredients'>
    <input type="text" name='units' placeholder='units'>
    <input type="text" name='amounts' placeholder='amounts'>
    
    <input name='title' placeholder='title'>
    <input name='portions' placeholder='portions'>
    <input name='preptime' placeholder='preptime'>
    <input name='difficulty' placeholder='difficulty'>
    <input name='origin' placeholder='origin'>
    <input type="text" name='description' placeholder='description'>
    <input type="text" name='preparation' placeholder='preparation'>
    <input name='title' placeholder='title'>

    <input type='submit' />
    </form>
    <a>
  `)
})


