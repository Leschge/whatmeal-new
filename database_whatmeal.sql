/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE IF NOT EXISTS `whatmealdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `whatmealdb`;

CREATE TABLE IF NOT EXISTS `fridge` (
  `userID` int(11) NOT NULL,
  `ingredientID` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `expirationdate` date DEFAULT NULL,
  PRIMARY KEY (`userID`,`ingredientID`),
  KEY `fridgeID` (`userID`),
  KEY `ingredientID` (`ingredientID`),
  CONSTRAINT `ingredientID_fridge` FOREIGN KEY (`ingredientID`) REFERENCES `ingredient` (`ingredientID`),
  CONSTRAINT `userID_fridge` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `fridge`;
/*!40000 ALTER TABLE `fridge` DISABLE KEYS */;
/*!40000 ALTER TABLE `fridge` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `has` (
  `recipeID` int(11) NOT NULL,
  `tagID` int(11) NOT NULL,
  PRIMARY KEY (`recipeID`,`tagID`),
  KEY `recipeID` (`recipeID`),
  KEY `tagID` (`tagID`),
  CONSTRAINT `recipeID_has` FOREIGN KEY (`recipeID`) REFERENCES `recipe` (`recipeID`),
  CONSTRAINT `tagID_has` FOREIGN KEY (`tagID`) REFERENCES `tag` (`tagID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `has`;
/*!40000 ALTER TABLE `has` DISABLE KEYS */;
INSERT INTO `has` (`recipeID`, `tagID`) VALUES
	(1, 3),
	(2, 3);
/*!40000 ALTER TABLE `has` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `ingredient` (
  `ingredientID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `unit` varchar(32) NOT NULL,
  PRIMARY KEY (`ingredientID`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

DELETE FROM `ingredient`;
/*!40000 ALTER TABLE `ingredient` DISABLE KEYS */;
INSERT INTO `ingredient` (`ingredientID`, `name`, `unit`) VALUES
	(1, 'Kuddla', 'gramm'),
	(2, 'Brot', 'pcs'),
	(3, 'Schmalz', 'gramm'),
	(4, '1', '2'),
	(5, 'Wurst', 'pcs'),
	(6, 'Gurke', 'pcs');
/*!40000 ALTER TABLE `ingredient` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `needs` (
  `recipeID` int(11) NOT NULL,
  `ingredientID` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`recipeID`,`ingredientID`),
  KEY `recipeID` (`recipeID`),
  KEY `ingredientID` (`ingredientID`),
  CONSTRAINT `ingredientID_needs` FOREIGN KEY (`ingredientID`) REFERENCES `ingredient` (`ingredientID`),
  CONSTRAINT `recipeID_needs` FOREIGN KEY (`recipeID`) REFERENCES `recipe` (`recipeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `needs`;
/*!40000 ALTER TABLE `needs` DISABLE KEYS */;
INSERT INTO `needs` (`recipeID`, `ingredientID`, `amount`) VALUES
	(1, 1, 500),
	(2, 2, 1),
	(2, 3, 100);
/*!40000 ALTER TABLE `needs` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `rating` (
  `userID` int(11) NOT NULL,
  `recipeID` int(11) NOT NULL,
  `stars` int(1) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `favourite` bit(1) DEFAULT NULL,
  PRIMARY KEY (`userID`,`recipeID`),
  KEY `userID` (`userID`),
  KEY `recipeID` (`recipeID`),
  CONSTRAINT `recipeID_rating` FOREIGN KEY (`recipeID`) REFERENCES `recipe` (`recipeID`),
  CONSTRAINT `userID_rating` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `rating`;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
INSERT INTO `rating` (`userID`, `recipeID`, `stars`, `date`, `favourite`) VALUES
	(1, 2, 4, '2019-12-06', b'0'),
	(39, 3, 6, NULL, NULL);
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `recipe` (
  `recipeID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) DEFAULT 1,
  `title` varchar(64) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `portions` int(4) DEFAULT 1,
  `preparation` varchar(1024) DEFAULT NULL,
  `image` mediumtext DEFAULT NULL,
  `preptime` int(4) DEFAULT NULL,
  `difficulty` int(4) DEFAULT NULL,
  `origin` varchar(128) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`recipeID`),
  KEY `userID` (`userID`),
  CONSTRAINT `userID_recipe` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

DELETE FROM `recipe`;
/*!40000 ALTER TABLE `recipe` DISABLE KEYS */;
INSERT INTO `recipe` (`recipeID`, `userID`, `title`, `description`, `portions`, `preparation`, `image`, `preptime`, `difficulty`, `origin`, `date`) VALUES
	(1, 1, 'default', 'default', 1, '', NULL, 0, 0, '', '2019-01-01'),
	(2, NULL, 'Schmalzbrot', 'Schmalzbrot', 2, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 39, 'cybg', 'dfg', 5, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `recipe` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `session` (
  `SessionID` varchar(256) NOT NULL,
  `userID` int(11) NOT NULL DEFAULT 0,
  `expiration` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`SessionID`),
  KEY `UserID` (`userID`),
  CONSTRAINT `userID_session` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `session`;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` (`SessionID`, `userID`, `expiration`) VALUES
	('fafnONQ4sxvWA6dEXmtkslXkCuu2rGUaYDoe', 65, '2019-12-11 10:39:35');
/*!40000 ALTER TABLE `session` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `tag` (
  `tagID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`tagID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

DELETE FROM `tag`;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` (`tagID`, `name`) VALUES
	(1, 'heiß und fettig'),
	(2, 'nothing to do with pizzagate'),
	(3, 'tastes well');
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `user` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `pwhash` varchar(256) NOT NULL,
  `salt` varchar(64) NOT NULL,
  `avatar` mediumtext DEFAULT NULL,
  `about` varchar(1024) DEFAULT NULL,
  `experience` int(2) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `mail` (`mail`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`userID`, `mail`, `username`, `pwhash`, `salt`, `avatar`, `about`, `experience`, `birthday`) VALUES
	(1, 'default', 'defaultuser', 'default', 'default', '(NULL)', NULL, 0, '2019-01-01'),
	(39, 'dsfg', 'dfsg', 'dsfg', 'sdfg', NULL, NULL, NULL, NULL),
	(65, 'fdsgsdg@sdgdsfg', 'testuser', 'c52109434f5231cc1f66e71ae6196d986558f64203f29353b29accd6e801bdd2', '3yIbLmkYXlOeUOI06JQi9rGuxtfD4niljT8LhaNWOL8pxIdcYBUAFulXosirDxgO', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
