import { Component, OnInit } from '@angular/core';
import { Router , ActivatedRoute } from "@angular/router";
import { HttpRequestsService } from "../http-requests.service";
import { Rating } from "../rating";
import { Recipe } from "../recipe";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private httpservice: HttpRequestsService, private router: Router, private route: ActivatedRoute) { }

  profileID: number;
  profile: any;
  ratings: Rating[];
  recipes: Recipe[];
  symDiff = [ "", "beginner", "experienced" , "expert"];

  ngOnInit() {
    this.profileID = +this.route.snapshot.paramMap.get("userID");
    this.getProfileData();
    this.getProfileRatings();
    this.getProfileRecipes();
  }


  getProfileData(){
    this.httpservice.getProfile(this.profileID).subscribe(prof =>
      {
          this.profile = prof[0];
      })
  }

  getProfileRecipes(){
    this.httpservice.getRecipesUser(this.profileID).subscribe(rec =>
      {
        this.recipes = rec;
      })
  }

  getProfileRatings()
  {
    this.httpservice.getRatingsUser(this.profileID).subscribe(rat =>
      {
        this.ratings = rat;
      })
  }


}
