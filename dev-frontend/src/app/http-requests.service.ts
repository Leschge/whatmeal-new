import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/Http";
import { Ingredient } from "./ingredient";
import { Observable, of } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class HttpRequestsService {

  constructor(private http: HttpClient) { }

  private ingredientsUrl = "api/ingredient/getAll";
  private loginUrl = "api/user/login";
  private logoutUrl = "api/user/logout";
  private registerUrl = "api/user/register";
  private getUserIdUrl = "api/user/getMyUserID";
  private updateUserUrl = "/api/user/modify";
  private getFridgeUrl = "api/fridge/get/";
  private addToFridgeUrl = "/api/fridge/add";
  private changeAmountFridgeUrl = "/api/fridge/reduce/";
  private removeFromFridgeUrl = "/api/fridge/del/";
  private getRecipesUrl = "/api/recipe/getAll";
  private getRecipesUserUrl = "/api/recipe/fromUser/";
  private getRecipeIngredsUrl = "/api/recipe/getIngredients/";
  private getRecipeUrl = "/api/recipe/get/";
  private getRatingUrl = "/api/rating/getAll/";
  private getRecipeStarsUrl = "/api/recipe/getRating/";
  private getRatingUserUrl = "/api/rating/getUser/";
  private addRatingUrl = "/api/rating/add";
  private getProfileUrl = "/api/user/get/";
  private createRecipeUrl = "/api/recipe/addOrUpdate";



  getIngredients(): Observable<Ingredient[]>{
   return this.http.get<Ingredient[]>(this.ingredientsUrl)
            .pipe(catchError(this.handleError<Ingredient[]>("getIngredients",[])));
  }

  // LOGIN/LOGOUT FUNCTIONS //

  login(username,password): Observable<any>{
    return this.http.post<any>(this.loginUrl,{username, password})
      .pipe(catchError(this.handleError<any>("login")));
  }

  logout(): Observable<any>{
    return this.http.post<any>(this.logoutUrl,{})
      .pipe(catchError(this.handleError<any>("login")));
  }


  // USER FUNCTIONS //
  updateUser(mail,username,avatar,about,experience,birthday): Observable<any>{

    return this.http.post<any>(this.updateUserUrl,{mail,username,avatar,about,experience,birthday})
      .pipe(catchError(this.handleError<any>("updateUser")));
  }

  getUserId(): Observable<any>{
    return this.http.get<any>(this.getUserIdUrl)
      .pipe(catchError(this.handleError<any>("getUserId")));
  }

  getProfile(userID): Observable<any>{
    return this.http.get<any>(this.getProfileUrl+userID)
      .pipe(catchError(this.handleError<any>("getProfile")));
  }

  // FRIDGE FUNCTIONS //

  getFridge(userID): Observable<any>{
    return this.http.get<any>(this.getFridgeUrl+userID)
      .pipe(catchError(this.handleError<any>("getFridge")));
  }

  addToFridge(ingredientID,amount): Observable<any>{
    return this.http.post<any>(this.addToFridgeUrl,{ingredientID,amount})
  }

  removeFromFridge(ingredientID): Observable<any>{
    return this.http.post<any>(this.removeFromFridgeUrl+ingredientID,{})
  }

  changeAmountFridge(ingredientID,amount): Observable<any>{
    return this.http.get<any>(this.changeAmountFridgeUrl+ingredientID+"/"+amount)
      .pipe(catchError(this.handleError<any>("changeAmountFridge")));
  }

  // REGISTER FUNCTIONS //

  register(inputemail,inputuser,inputpw): Observable<any>{
    return this.http.post<any>(this.registerUrl,{email: inputemail, username: inputuser, password:inputpw})
      .pipe(catchError(this.handleError<any>("register")));
  }

  // RECIPE FUNCTIONS //

  getRecipes(){
    return this.http.get<any>(this.getRecipesUrl)
      .pipe(catchError(this.handleError<any>("getRecipes")));
  }

  getRecipesUser(userID){
    return this.http.get<any>(this.getRecipesUserUrl+userID)
      .pipe(catchError(this.handleError<any>("getRecipesUser")));
  }

  getRecipe(recipeID){
    return this.http.get<any>(this.getRecipeUrl+recipeID)
      .pipe(catchError(this.handleError<any>("getRecipe "+recipeID)));
  }

  getRecipeIngreds(recipeID){
    return this.http.get<any>(this.getRecipeIngredsUrl+recipeID)
      .pipe(catchError(this.handleError<any>("getRecipeIngreds "+recipeID)));
  }

  getRecipeStars(recipeID){
    return this.http.get<any>(this.getRecipeStarsUrl+recipeID)
      .pipe(catchError(this.handleError<any>("getRecipeStars")));
  }

  createRecipe(title,description,portions,preparation,image,preparation_time,difficulty,origin,ingredients: any[],tags: any[]): Observable<any>{
    return this.http.post<any>(this.createRecipeUrl,
      {
        title, description, portions, preparation,image,preparation_time,difficulty,origin,ingredients,tags
      }
      )
      .pipe(catchError(this.handleError<any>("createRecipe")));
  }

  // RATING FUNCTIONS //

  getRatings(recipeID){
    return this.http.get<any>(this.getRatingUrl+recipeID)
      .pipe(catchError(this.handleError<any>("getRatings "+recipeID)));
  }

  getRatingsUser(userID){
    return this.http.get<any>(this.getRatingUserUrl+userID)
      .pipe(catchError(this.handleError<any>("getRatings "+userID)));
  }

  addRating(recipeID, stars): Observable<any>{
    return this.http.post<any>(this.addRatingUrl,{recipeID, stars})
  }

  // ERROR //

  private handleError<T> (operation = "operation", result?:T){
    return (error: any): Observable<T> =>{
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    }
  }

}

