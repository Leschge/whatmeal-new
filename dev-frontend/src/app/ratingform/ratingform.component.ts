import { Component, OnInit , Input} from '@angular/core';
import { HttpRequestsService } from "../http-requests.service";

@Component({
  selector: 'app-ratingform',
  templateUrl: './ratingform.component.html',
  styleUrls: ['./ratingform.component.css']
})
export class RatingformComponent implements OnInit {

  constructor(private httpService: HttpRequestsService) { }

  stars = 0;
  errormessage = "";
  succmessage = false;

  ngOnInit() {
  }

  @Input() recipeID: number;

  sendRating(){
    this.httpService.addRating(this.recipeID,this.stars).subscribe(res =>
      {
        if(res.status)
        {
          if(res.status == -1)
          {
            this.errormessage = res.message;
          }
          else
          {
            this.succmessage = true;
            this.errormessage = "";
          }
        }
      })
  }

}
