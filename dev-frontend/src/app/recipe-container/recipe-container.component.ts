import { Component, OnInit } from '@angular/core';

import { HttpRequestsService } from "../http-requests.service";
import { RecipedataService } from "../recipedata.service";
import { PRODUCTS, setFridge } from "../fridge";

import { Recipe } from "../recipe";
import { Ingredient } from '../ingredient';

@Component({
  selector: 'app-recipe-container',
  templateUrl: './recipe-container.component.html',
  styleUrls: ['./recipe-container.component.css']
})
export class RecipeContainerComponent implements OnInit {

  buttonAvailable;

  allRecipes: Recipe[];
  allRecipesCopies: Recipe[];

  ingredientsLoaded = false;

  searchRecipeTerm = "";

  maxDur = 180;
  selDur = this.maxDur;

  maxDiff = 3;
  selDiff = this.maxDiff;
  symDiff = [ "", "beginner", "experienced" , "expert"];

  constructor(private httpService: HttpRequestsService, private recipedata: RecipedataService) {
    this.getRecipes();
   }

  ngOnInit() {
    this.buttonAvailable = <HTMLInputElement> document.getElementById('available-selector');
    this.buttonAvailable.checked = false;
  }

  getRecipes(){
    this.httpService.getRecipes().subscribe(rec =>
      {
        this.allRecipes = rec;
        this.allRecipesCopies = rec;
      });
  }

  searchRecipe():void{
    let available = this.buttonAvailable.checked;

    let term = this.searchRecipeTerm;
    let dur = this.selDur;
    let diff = this.selDiff;

    let ingredientFound, hasAllIngredients;
    var possibleRecipes: Recipe[] = [];

    if(available)
    {
      for(let singleRecipe of this.allRecipesCopies)
      {
        hasAllIngredients = true;
        if( singleRecipe.ingredients)
        {
          for(let recIng of singleRecipe.ingredients)
          {
            ingredientFound = false;
            for(let fridgeProd of PRODUCTS)
            {
              if(fridgeProd.ingredientID == recIng.ingredientID && fridgeProd.amount >= recIng.amount)
              {
                ingredientFound = true;
              }
            }
            if(!ingredientFound)
            {
              hasAllIngredients = false;
            }
          }
        }
        if(hasAllIngredients)
        {
          possibleRecipes.push(singleRecipe);
        }
      }

      this.allRecipes = possibleRecipes.filter(function(obj){
        return ( obj.title.toLowerCase().indexOf(term) >= 0 || obj.origin.indexOf(term) >= 0 ) && (obj.preptime <= dur && obj.difficulty <= diff );
      })
    }
    else
    {
      this.allRecipes = this.allRecipesCopies.filter(function(obj){
        return ( obj.title.toLowerCase().indexOf(term) >= 0 || obj.origin.indexOf(term) >= 0 ) && (obj.preptime <= dur && obj.difficulty <= diff );
      })
    }

  }

  filterAvailability():void{
  
    let availabilityChecked = this.buttonAvailable.checked; 

    if(availabilityChecked)
    {
      if(this.ingredientsLoaded == false)
      {
        for (let rec of this.allRecipesCopies) {
          this.httpService.getRecipeIngreds(rec.recipeID).subscribe(ings =>
            {
              this.allRecipesCopies.find(x => x.recipeID == rec.recipeID).ingredients = ings;
              this.searchRecipe();
              this.ingredientsLoaded = true;
            });
        }
      }
      else{
        this.searchRecipe();
      }
    }
    else{
      this.searchRecipe();
    }

  }

  filterDuration(dur: number):void{
    this.selDur = dur;
    this.searchRecipe();
  }

  filterDifficulty(diff: number):void{
    this.selDiff = diff;
    this.searchRecipe();
  }

}
