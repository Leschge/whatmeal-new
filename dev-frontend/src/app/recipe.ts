export class Recipe{
    recipeID: number;
    title: string;
    description: string;
    ingredients: {
        ingredientID: number,
        amount: number,
        name: String,
        unit: String
    }[];
    preparation: string;
    image: string;
    preptime: number;
    difficulty: number;
    origin: string;
    tags: string[];
    date: string;
    user: number;
}