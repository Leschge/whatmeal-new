import { Component, OnInit, Input, SimpleChange } from '@angular/core';

@Component({
  selector: 'app-stars',
  templateUrl: './stars.component.html',
  styleUrls: ['./stars.component.css']
})
export class StarsComponent implements OnInit {

  col = [ "gray","gray","gray","gray","gray" ];

  constructor() { }

  ngOnInit() {
    let i = 0;

    while(i < Math.round(this.stars))
    {
      this.col[i] = "orange";
      i++;
    }
  }

  ngOnChanges(){
    let i = 0;

    while(i < Math.round(this.stars))
    {
      this.col[i] = "orange";
      i++;
    }
  }

  @Input() stars:number;

}
