import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientRecipeComponent } from './ingredient-recipe.component';

describe('IngredientRecipeComponent', () => {
  let component: IngredientRecipeComponent;
  let fixture: ComponentFixture<IngredientRecipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngredientRecipeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientRecipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
