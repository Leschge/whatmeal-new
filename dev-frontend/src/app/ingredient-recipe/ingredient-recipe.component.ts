import { Component, OnInit, Input } from '@angular/core';

import { Ingredient } from "../ingredient";

@Component({
  selector: 'app-ingredient-recipe',
  templateUrl: './ingredient-recipe.component.html',
  styleUrls: ['./ingredient-recipe.component.css']
})
export class IngredientRecipeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input() ingredient: Ingredient[];

  

}
