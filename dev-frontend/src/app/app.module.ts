import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { FridgeComponent } from './fridge/fridge.component';
import { IngredientComponent } from './ingredient/ingredient.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { IngredientAddComponent } from './ingredient-add/ingredient-add.component';
import { RecipeComponent } from './recipe/recipe.component';
import { RecipeContainerComponent } from './recipe-container/recipe-container.component';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';
import { IngredientRecipeComponent } from './ingredient-recipe/ingredient-recipe.component';
import { RatingComponent } from './rating/rating.component';
import { StarsComponent } from './stars/stars.component';
import { ProfileComponent } from './profile/profile.component';
import { MeComponent } from './me/me.component';
import { IngredientAddrecComponent } from './ingredient-addrec/ingredient-addrec.component';
import { RatingformComponent } from './ratingform/ratingform.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterFormComponent,
    LoginFormComponent,
    FridgeComponent,
    IngredientComponent,
    MainpageComponent,
    IngredientAddComponent,
    RecipeComponent,
    RecipeContainerComponent,
    RecipeDetailComponent,
    IngredientRecipeComponent,
    RatingComponent,
    StarsComponent,
    ProfileComponent,
    MeComponent,
    IngredientAddrecComponent,
    RatingformComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
