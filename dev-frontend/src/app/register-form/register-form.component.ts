import { Component, OnInit } from '@angular/core';
import { HttpRequestsService } from "../http-requests.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  constructor(private httpService: HttpRequestsService, private router: Router) { }

  email = "";
  username = "";
  password = "";
  passwordrep = "";
  registermessage;

  UrlLoginPage = "/account";

  ngOnInit() {
  }

  register(){
    if(this.password != this.passwordrep)
    {
      this.registermessage = "Passwords not matching";
    }
    else
    {
        this.httpService.register(this.email,this.username,this.password).subscribe(res =>
        {
            if(res.status) 
            { 
              if(res.status == -1)
              {
                 this.registermessage = res.message;
              }

              if(res.status == 1)
              {
                this.router.navigate([this.UrlLoginPage]);
              }
            }

          }
        )
    }

  }

}
