import { Component, OnInit } from '@angular/core';
import { HttpRequestsService } from "../http-requests.service";
import { Router } from "@angular/router";
import { USER_ID, setUserID } from "../userID";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  constructor(private httpService: HttpRequestsService, private router: Router) { }

  username = "";
  password = "";

  loginmessage = false;

  UrlProfile = "/account/me";
  UrlMainpage = "/";
  UrlLoginPage = "/account";

  ngOnInit() {
    if(USER_ID != -1 && this.router.url == this.UrlLoginPage)
    {
      this.router.navigate([this.UrlProfile]);
    }
  }

  login(){
    this.httpService.login(this.username,this.password).subscribe( res => 
      {
        console.log(res);

        if(res.userID)
        {
          if(res.userID != -1)
          {
            setUserID(res.userID);
            this.closeModal();
            //Check if logged in via modal on mainpage
            if(this.router.url == this.UrlMainpage)
            {
              window.location.reload();
            }
            else
            {
              this.router.navigate([this.UrlProfile]);
            }
          }
        }

        if(res.message)
        {
          this.loginmessage = res.message;
        }

        if(res.error) 
        { 
          if(res.error.indexOf('We will redirect you, because you are already logged in.') > -1) { this.getUserId(); } 
        }

      }
    );
  }

  getUserId(){
    this.httpService.getUserId().subscribe( res => 
      {
        setUserID(res.userID);
        this.closeModal();
        //Check if logged in via modal on mainpage
        if(this.router.url == this.UrlMainpage)
        {
          window.location.reload();
        }
        else
        {
          this.router.navigate([this.UrlProfile]);
        }
      }
    );}


    closeModal():void{
      let modal = document.getElementById('modalLogin');
      if(modal) { modal.style.display = "none"; }
  
      const modalback = Array.from(document.getElementsByClassName('modal-backdrop fade show'));
      for (const x of modalback) {
          const y = <HTMLElement> x;
          y.style.display = 'none';
      }
    }

}
