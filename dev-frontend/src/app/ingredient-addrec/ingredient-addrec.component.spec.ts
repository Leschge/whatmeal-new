import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientAddrecComponent } from './ingredient-addrec.component';

describe('IngredientAddrecComponent', () => {
  let component: IngredientAddrecComponent;
  let fixture: ComponentFixture<IngredientAddrecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngredientAddrecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientAddrecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
