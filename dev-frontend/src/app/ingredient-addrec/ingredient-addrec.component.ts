import { Component, OnInit , Input, Output, EventEmitter } from '@angular/core';

import { Ingredient } from "../ingredient";
import { HttpRequestsService } from "../http-requests.service";

@Component({
  selector: 'app-ingredient-addrec',
  templateUrl: './ingredient-addrec.component.html',
  styleUrls: ['./ingredient-addrec.component.css']
})
export class IngredientAddrecComponent implements OnInit {

  amount = 10;

  constructor(private httpService: HttpRequestsService) { }

  ngOnInit() {
    switch(this.ingredient.unit){
      case "g":
        this.amount = 100;
      break;
      case "ml":
        this.amount = 500;
      break;
      case "pcs":
        this.amount = 3;
      break;
      case "g":
        this.amount = 100;
      break;

    }
  }

  @Input() ingredient: Ingredient;

  @Output() callingredientToRecipe = new EventEmitter<any[]>();

  addItemToRecipe(){
    let ingRecipe = [this.ingredient.ingredientID,this.amount];
    console.log(ingRecipe);
    this.callingredientToRecipe.next(ingRecipe);
  }

}
