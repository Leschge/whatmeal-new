import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpRequestsService } from "../http-requests.service";

import { Product } from "../product"; // item is combination of ingredient and

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent implements OnInit {

  constructor(private httpService: HttpRequestsService) { }

  initialAmount: number;

  ngOnInit() {
    this.initialAmount = this.product.amount;
  }

  @Input() product: Product;
  @Output() refreshFridge = new EventEmitter<string>();


  fridgeUpdateAmount(){
     this.httpService.changeAmountFridge(this.product.ingredientID,(this.initialAmount-this.product.amount)).subscribe(res =>
      {
        this.refreshFridge.next();
      })
  }


  delProductFromFridge(){
    this.httpService.removeFromFridge(this.product.ingredientID).subscribe(res =>
      {
        this.refreshFridge.next();
      })
  }

}
