import { Component, OnInit , Input, Output, EventEmitter } from '@angular/core';

import { Ingredient } from "../ingredient";
import { HttpRequestsService } from "../http-requests.service";

@Component({
  selector: 'app-ingredient-add',
  templateUrl: './ingredient-add.component.html',
  styleUrls: ['./ingredient-add.component.css']
})
export class IngredientAddComponent implements OnInit {

  amount = 10;

  constructor(private httpService: HttpRequestsService) { }

  ngOnInit() {
    switch(this.ingredient.unit){
      case "g":
        this.amount = 100;
      break;
      case "ml":
        this.amount = 500;
      break;
      case "pcs":
        this.amount = 3;
      break;
      case "g":
        this.amount = 100;
      break;

    }
  }

  @Input() ingredient: Ingredient;

  @Output() refreshFridge = new EventEmitter<string>();

  addItemToFridge(){
    this.httpService.addToFridge(this.ingredient.ingredientID,this.amount).subscribe(res =>
      {
         this.refreshFridge.next();
      })
  }

}
