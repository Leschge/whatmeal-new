import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Recipe } from "./recipe";

@Injectable({
  providedIn: 'root'
})
export class RecipedataService {

  initRec: Recipe[]
  allRecipes = new BehaviorSubject(this.initRec);
  currentRecipes = this.allRecipes.asObservable();

  constructor() { 
  }

  updateRecipes(newRecipes: Recipe[]){
    this.allRecipes.next(newRecipes);
  }

}
