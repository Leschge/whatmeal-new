// Combination of ingredient.ts (database ingredient) and product.ts (fridge ingredient)

export class Item {
    ID: Number;
    name: String;
    unit: String;
    category: String;
    ingredient: Number;
    amount: Number;
    expires: String;
  }