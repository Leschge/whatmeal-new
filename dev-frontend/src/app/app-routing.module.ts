import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegisterFormComponent } from "./register-form/register-form.component";
import { LoginFormComponent } from "./login-form/login-form.component";
import { MainpageComponent } from "./mainpage/mainpage.component";
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';
import { MeComponent } from "./me/me.component";
import { ProfileComponent } from "./profile/profile.component";


const routes: Routes = [
  { path: '', component: MainpageComponent },
  { path: 'register', component: RegisterFormComponent },
  { path: 'account', component: LoginFormComponent },
  { path: 'account/me', component: MeComponent },
  { path: 'profile/:userID', component: ProfileComponent},
  { path: 'login', component: LoginFormComponent },
  { path: 'recipe/:recipeID', component: RecipeDetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }