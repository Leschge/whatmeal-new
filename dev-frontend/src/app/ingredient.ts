export class Ingredient {
    ingredientID: Number;
    name: String;
    unit: String;
  }
