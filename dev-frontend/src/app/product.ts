export class Product {
    ingredientID: number;
    name: String;
    amount: number;
    unit: String;
    expirationdate: String;
  }