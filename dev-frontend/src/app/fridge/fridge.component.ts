import { Component, OnInit } from '@angular/core';

import { Product } from "../product";
import { Ingredient } from '../ingredient';

import { HttpRequestsService } from "../http-requests.service";
import { USER_ID, setUserID } from "../userID";
import { PRODUCTS, setFridge } from "../fridge";

@Component({
  selector: 'app-fridge',
  templateUrl: './fridge.component.html',
  styleUrls: ['./fridge.component.css']
})

export class FridgeComponent implements OnInit {

  userID;
  notloggedIn = true;

  allProducts:Product[]; // All products (ingredients) from users fridge
  allIngredients:Ingredient[]; // = mockIngredient.ingredient; // All possible ingredients from database
  
  allProductsCopy;
  allIngredientsCopy;

  searchFridgeTerm = "";
  searchIngredientTerm = "";

  loadingFridge = true;
  loadingIngredients = true;

  constructor(private httpService: HttpRequestsService) { }

  ngOnInit() {
    this.getUser();
    this.getIngredients();
  }

  getUser():void{
    this.httpService.getUserId().subscribe(res =>
      {
        if(res.userID)
        {
          if(res.userID != -1)
          {
            setUserID(res.userID);
            this.userID = res.userID;
            this.notloggedIn = false;
            this.getFridge();
          }
          else{
            console.log("User not logged in. (-1)");
            this.loadingFridge = false;
          }
        }
        else
        {
          console.log("User not logged in.");
          this.loadingFridge = false;
        }
        
      })
  }

  getFridge():void{
    this.httpService.getFridge(this.userID).subscribe(prod =>
      {
        this.allProducts = prod;
        setFridge(this.allProducts);
        this.allProductsCopy = this.allProducts;
        this.loadingFridge = false;
      })
  }

  getIngredients():void{
    this.httpService.getIngredients().subscribe(ings => 
      { 
        this.allIngredients = ings;
        this.allIngredientsCopy = this.allIngredients;
        this.loadingIngredients = false;
      });
  }

  searchFridge():void{
      let term = this.searchFridgeTerm;
      this.allProducts = this.allProductsCopy.filter(function(obj){
        return obj.name.indexOf(term) >= 0;
      })
  }

  searchIngredient():void{
    let term = this.searchIngredientTerm;
    this.allIngredients = this.allIngredientsCopy.filter(function(obj){
      return obj.name.indexOf(term) >= 0;
    })
  }


}
