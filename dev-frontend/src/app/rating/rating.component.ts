import { Component, OnInit, Input } from '@angular/core';

import { Rating } from "../rating";

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input() rating: Rating;

}
