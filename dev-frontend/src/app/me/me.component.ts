import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"
import { USER_ID, setUserID } from "../userID";
import { HttpRequestsService } from "../http-requests.service";
import { Rating } from "../rating";
import { Recipe } from "../recipe";
import { Ingredient } from '../ingredient';


@Component({
  selector: 'app-profile',
  templateUrl: './me.component.html',
  styleUrls: ['./me.component.css']
})
export class MeComponent implements OnInit {

  constructor(private httpservice: HttpRequestsService, private router: Router) { }

  errormessage;
  succmessage = false;

  searchIngredientTerm;
  allIngredients:Ingredient[];
  allIngredientsCopy:Ingredient[];


  profileID: number;
  profile: any;
  ratings: Rating[];
  recipes: Recipe[];
  symDiff = [ "", "beginner", "experienced" , "expert"];

  file;
  imageToLarge = false;

  editAvatar;
  editExperience;
  editAbout;
  editBirthday;


  dishIngredients: any[] = [];
  addedIngredients: any[] = [];
  dishTags: any[] = [];

  dishTitle;
  dishDescription;
  dishPreparation;
  dishPortions;
  dishPrepTime;
  dishOrigin;
  dishDifficulty;
  dishPicture;

  ngOnInit() {

    if(USER_ID == -1)
    {
      this.router.navigate(["/account"]);
      console.log("Please log in to watch your profile.")
    }
    else
    {
      this.getProfileData();
      this.getProfileRatings();
      this.getProfileRecipes();
      this.getIngredients();
    }
  }

  onFileChange(event){
    let imagefile = event.target.files[0];
    console.log(imagefile.size);
    if(imagefile.size > 50000)
    {
      this.imageToLarge = true;
    }
    else
    {
      this.imageToLarge = false;
      this.getBase64(imagefile,0);
    }
  }

  onFileChangeRec(event){
    let imagefile = event.target.files[0];
    console.log(imagefile.size);
    if(imagefile.size > 256000)
    {
      this.imageToLarge = true;
    }
    else
    {
      this.imageToLarge = false;
      this.getBase64(imagefile,1);
    }
  }


  getBase64(file,what) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (e) => {
      if(what == 0) { this.editAvatar = reader.result; }
      if(what == 1) { this.dishPicture = reader.result; }
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }



  updateProfile(){
    this.httpservice.updateUser(this.profile.mail,this.profile.username,this.editAvatar,this.editAbout.replace(/'/g, "´"),this.editExperience,this.editBirthday).subscribe(res => {
      this.getProfileData();
    })
  }


  createRecipe(){


 console.log(this.dishTitle)
 console.log(this.dishDescription)
 console.log(this.dishPreparation)
 console.log(this.dishPortions)
 console.log(this.dishPrepTime)
 console.log(this.dishOrigin)
 console.log(this.dishDifficulty)
 console.log(this.dishPicture)
 console.log(this.dishIngredients)
 console.log(this.dishTags)

    
    this.httpservice.createRecipe(this.dishTitle,this.dishDescription,this.dishPortions,this.dishPreparation,this.dishPicture,this.dishPrepTime,this.dishDifficulty,this.dishOrigin,this.dishIngredients,this.dishTags).subscribe(res => {
      if(res.status)
      {
        if(res.status == 1)
        {
          this.getProfileRecipes()
          this.succmessage = true;
        }
        else
        {
          this.errormessage = res.message;
        }
      }
    })
  }


  ingredientToRecipe(event){
    if(!this.dishIngredients.find(x => x.ingredient == event[0]))
    {
      this.dishIngredients.push({ingredient: event[0], amount: event[1]});
      let ingA = this.allIngredientsCopy.find(x => x.ingredientID == event[0])
      this.addedIngredients.push([ingA.name,event[1],ingA.unit])
    }else{
      console.log("Ingredient already in recipe");
    }
  }


  logout(){
    this.httpservice.logout().subscribe(logout =>
      {
        if(logout.status == 1)
        {
            setUserID(-1);
            this.router.navigate(["/"]);
        }
        else{
          console.log("LOGOUT FAILED: "+logout.message)
        }
      });
  }


  getIngredients():void{
    this.httpservice.getIngredients().subscribe(ings => 
      { 
        this.allIngredients = ings;
        this.allIngredientsCopy = this.allIngredients;
      });
  }

  searchIngredient():void{
    let term = this.searchIngredientTerm;
    this.allIngredients = this.allIngredientsCopy.filter(function(obj){
      return obj.name.indexOf(term) >= 0;
    })
  }


  getProfileData(){
    this.httpservice.getProfile(USER_ID).subscribe(prof =>
      {
          this.profile = prof[0];

          this.editAvatar = this.profile.avatar;
          this.editAbout = this.profile.about;
          this.editExperience = this.profile.experience;
          this.editBirthday = this.profile.birthday.substring(0,10);
      })
  }

  getProfileRecipes(){
    this.httpservice.getRecipesUser(USER_ID).subscribe(rec =>
      {
        this.recipes = rec;
      })
  }

  getProfileRatings()
  {
    this.httpservice.getRatingsUser(USER_ID).subscribe(rat =>
      {
        this.ratings = rat;
      })
  }




}
