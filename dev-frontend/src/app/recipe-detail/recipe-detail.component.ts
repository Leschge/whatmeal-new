import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { HttpRequestsService } from "../http-requests.service";
import { RecipedataService } from "../recipedata.service";

import { Recipe } from "../recipe"
import { Ingredient } from "../ingredient";
import { Rating } from "../rating";

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {


  ID;
  allIngredients:Ingredient[];
  recipe: Recipe;
  tags: String[];
  ratings: Rating[];

  constructor(private route: ActivatedRoute,private httpService: HttpRequestsService, private recipedata: RecipedataService) { }

  ngOnInit() {

    //this.recipedata.currentRecipes.subscribe(rec => this.allRecipes = rec);

    this.ID = +this.route.snapshot.paramMap.get("recipeID");
    this.getRecipe(this.ID);
    this.getRatings(this.ID);
    this.getIngredients(this.ID);
  }

  getRecipe(id){
    this.httpService.getRecipe(id).subscribe(rec =>
      {
        this.recipe = rec[0];
      });
  }

  getIngredients(id){
    this.httpService.getRecipeIngreds(id).subscribe(ings =>
      {
        this.allIngredients = ings;
      })
  }

  getRatings(id){
    this.httpService.getRatings(id).subscribe(rec =>
      {
        this.ratings = rec;
      });
  }


}
