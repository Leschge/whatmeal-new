export class Rating {
    userID: number;
    recipeID: number;
    stars: number;
    date: String;
    favourite: boolean;
  }