'use strict';
import { Product } from "./product";

export var PRODUCTS : Product[];

export function setFridge(products: Product[]) {
    PRODUCTS = products;
}