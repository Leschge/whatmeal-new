﻿base url for database functions (http requests):
http://localhost:3000/api


_________________



########## recipe ##########

/recipe/add
PUT

________________________


/recipe/update/:id
PUT

required: user has to be logged in
	
not given parameters (optional) will not overwrite old parameters

{
	
	"title":
		"title of dish",
	"description":
		"short description of dish",
	"ingredients": 
		[
			{
				"ingredient": ingredientID,
				"amount": 3
			},
			{
				"ingredient": ingredientID,
				"amount": 10
			}
		],
	"portions":
	    4,
	"preparation": 
		"how you make it",
	"image":
		"base64 image string",
	"preparation_time":
		60,
	"difficulty":
		1-3,
	"origin":
		"asia",
	"tags":
		[
			1,
			4,
			5
		],
}

_________________
/recipe/fromUser/{userId}
GET

returns entire entry by given ID as json

__________________________


/recipe/get/{recId}
GET

returns entire entry by given ID as json

_________________

/recipe/getAll
GET

returns all recipes (fitting the logged in users fridge)   <- noch klären

_________________

/recipe/del/{id}
DELETE

required: has to be logged in, can only delete his own recipes

deletes entry by given ID

__________________

/recipe/getByTag/{tag}
GET

_________________









########## user ##########


/user/getMyUserID
GET

required: has to be logged in


____________________

/user/register
POST

required:
username, email, password

optional: avatar, about, experience, birthday

_________________

/user/get/{id}
GET

returns:
{
	"mail":
		"email-address",
	"username":
		"Niclas",
	"avatar":
		"base64 image string",
	"about":
		"I am a cool guy",
	"experience":
		1-10,
	"birthday":
		"date"
}



_________________

/user/login/

{username,hashedPW}
POST

returns 

_________________

/user/logout
POST

req: has to be logged in

returns 

_________________









########## rating ##########

/rating/add
POST

req: has to be logged in

recipeID, stars, favourite

(if recipe is already rated, rating gets updated)

_________________


/rating/getAll/:recID
GET

req: recipe id

returns all ratings of a recipe


_________________

/recipe/getUser/{userID}
GET

returns all entries related to given user ID





########## ingredient ##########

//add ingredient to database

/ingredient/add
POST

reqired: name, unit

_____________________

/ingredient/get/{ingredientID}

GET

_________________________

/ingredient/get/{name}

GET

________________________

/ingredient/getAll
GET





########## fridge ##########

/fridge/add
POST

- has to be logged in

required:
	ingredientID, amount
optional:
	expires



_________________

/fridge/get/{userID}
GET

returns entire entry by given ID as json

_________________

/fridge/del/{ingredientID}
DELETE

- has to be logged in

deletes product from fridge by given ingredient ID

_________________

/fridge/clear
DELETE

- has to be logged in

deletes entire fridge entry



